<?php
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();
//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if (!$obj_usuario->verifica_usuario_logado()):
    $caminho = Util::caminho_projeto() . "/locacao-de-equipamentos";
    header("location: $caminho ");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if (!isset($_SESSION[produtos])):
    $caminho = Util::caminho_projeto() . "/locacao-de-equipamentos";
    header("location: $caminho ");
endif;


# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if (isset($_POST[btn_cadastrar])):
    $obj_carrinho->armazena_mensagem_endereco_entrega($_POST);

    $caminho = Util::caminho_projeto() . "/pagamento";
    header("location: $caminho ");

endif;


// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/locacao-de-equipamentos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

    <?php require_once('./includes/js_css.php') ?>


</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>


<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top240">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[legenda_1]); ?></h1>
            <h3 class="ml-5"><?php Util::imprime($banner[legenda_2]); ?></h3>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top30">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a><?php Util::imprime($banner[legenda_1]); ?></a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<!-- ======================================================================= -->
<!--  DESCRICAO -->
<!-- ======================================================================= -->
<div class="container bottom50">
    <div class="row">

        <!-- ======================================================================= -->
        <!-- passo a passo    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/passo_a_passo.php') ?>
        <!-- ======================================================================= -->
        <!-- passo a passo    -->
        <!-- ======================================================================= -->


        <div class="col-9">

            <!--  ==============================================================  -->
            <!--endereco entrega-->
            <!--  ==============================================================  -->
            <div class="dicas_titulo">
                <h2>ENDEREÇO PARA <span>ENTREGA</span></h2>
            </div>

            <div class="fundo-formulario">
                <form class="form-inline FormCadastro " role="form" method="post" enctype="multipart/form-data">

                    <div class="col-12 top10">
                        <div class="form-group  input100">
                            <input type="text" name="nome_contato"
                                   class="form-control fundo-form input-lg input100" placeholder="NOME">
                        </div>
                    </div>

                    <div class="col-12"></div>

                    <div class="col-4 top10">
                        <div class="form-group  input100">
                            <input type="text" name="telefone_contato"
                                   class="form-control fundo-form input-lg input100" placeholder="TELEFONE">
                        </div>
                    </div>

                    <div class="col-4 top10">
                        <div class="form-group  input100">
                            <input type="text" id="celular" name="celular_contato"
                                   class="form-control fundo-form input-lg input100" placeholder="CELULAR">
                        </div>
                    </div>

                    <div class="col-4 top10">
                        <div class="form-group  input100">
                            <input type="number" min="8" maxlength="8" name="cep_entrega"
                                   class="form-control fundo-form input-lg input100" placeholder="CEP">
                        </div>
                    </div>


                    <div class="col-8 top10">
                        <div class="form-group  input100">
                            <input type="text" name="endereco_entrega"
                                   class="form-control fundo-form input-lg input100" placeholder="ENDEREÇO DE ENTREGA">
                        </div>
                    </div>

                    <div class="col-4 top10">
                        <div class="form-group  input100">
                            <input type="text" name="numero_entrega"
                                   class="form-control fundo-form input-lg input100" placeholder="NÚMERO">
                        </div>
                    </div>


                    <div class="col-12 top10">
                        <div class="form-group  input100">
                            <input type="text" name="complemento_entrega"
                                   class="form-control fundo-form input-lg input100" placeholder="COMPLEMENTO">
                        </div>
                    </div>


                    <div class="col-12 top10">
                        <div class="form-group  input100">
                            <input type="text" name="ponto_referencia"
                                   class="form-control fundo-form input-lg input100" placeholder="REFERÊNCIA">
                        </div>
                    </div>


                    <div class="col-5 top10">
                        <div class="form-group  input100">
                            <input type="text" name="bairro" disabled
                                   class="form-control fundo-form input-lg input100"
                                   placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo")); ?>">
                        </div>
                    </div>

                    <div class="col-5 top10">
                        <div class="form-group  input100">
                            <input type="text" name="cidade" disabled
                                   class="form-control fundo-form input-lg input100"
                                   placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_cidade], "tb_cidades", "idcidade", "titulo")); ?>">
                        </div>
                    </div>

                    <div class="col-2 top10">
                        <div class="form-group  input100">
                            <input type="text" name="uf" disabled class="form-control fundo-form input-lg input100"
                                   placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_cidade], "tb_cidades", "idcidade", "uf")); ?>">
                        </div>
                    </div>


                    <div class="col-6 dicas_titulo top15 ">
                        <a class="btn btn-cinza btn-lg" href="<?php echo Util::caminho_projeto() ?>/carrinho">
                            <h2> TROCAR LOCAL <span>DE ENTREGA</span></h2></a>
                    </div>


                    <div class="col-6 text-right">
                        <div class="top15 bottom25">
                            <button type="submit" class="btn btn-outline-secondary rounded-0 btn_detalhe_produtos" name="btn_cadastrar">
                                ENVIAR
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!--  ==============================================================  -->
            <!--endereco entrega-->
            <!--  ==============================================================  -->

        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!--  DESCRICAO -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<script>
    $(document).ready(function () {
        $('.FormCadastro').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                nome: {
                    validators: {
                        notEmpty: {}
                    }
                },


                nome_contato: {
                    validators: {
                        notEmpty: {}
                    }
                },




                telefone_contato: {
                    validators: {
                        notEmpty: {},
                        phone: {
                            country: 'BR',
                            message: 'Telefone inválido'
                        }
                    }
                },

                endereco_entrega: {
                    validators: {
                        notEmpty: {}
                    }
                },

                cep_entrega: {
                    validators: {
                        notEmpty: {}
                    }
                },


                numero_entrega: {
                    validators: {
                        notEmpty: {}
                    }
                },
                bairro: {
                    validators: {
                        notEmpty: {}
                    }
                },
                cidade: {
                    validators: {
                        notEmpty: {}
                    }
                },
                uf: {
                    validators: {
                        notEmpty: {}
                    }
                },


            }
        });

    });
</script>
