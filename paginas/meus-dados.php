<?php

$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( !$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/locacao-de-equipamentos";
  header("location: $caminho ");

endif;


if(isset($_POST[btn_atualizar])):

  $obj_usuario->atualiza_dados($_POST);
  Util::script_msg("Dados atualizados com sucesso.");

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
      <div class="row top240">
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-12 titulo_internas">
              <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
          </div>
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->

          <div class="col-4 top60">
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                              <i class="fas fa-home"></i> HOME
                          </a>
                      </li>
                      <li class="breadcrumb-item active"><a><?php Util::imprime($banner[titulo]); ?></a></li>
                  </ol>
              </nav>
          </div>

          <div class="col-8 top70 dicas_titulo">
              <h2><?php Util::imprime($banner[legenda_1]); ?> <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
          </div>

      </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container bottom50 top30">
      <div class="row">


          <!-- ======================================================================= -->
          <!-- passo a passo    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/area_clientes.php') ?>
          <!-- ======================================================================= -->
          <!-- passo a passo    -->
          <!-- ======================================================================= -->




          <div class="col-9 titulo_carrinho padding0">

            <!--  ==============================================================  -->
            <!--formulario-->
            <!--  ==============================================================  -->
            <div class="col-12  fundo-formulario">


              <form class="form-inline FormCadastro" role="form" method="post" enctype="multipart/form-data">

                <div class="col-12 ">
                  <div class="form-group  input100">
                    <input type="text" name="nome" value="<?php Util::imprime($_SESSION[usuario][nome]) ?>" class="form-control fundo-form input-lg input100" placeholder="NOME">
                  </div>
                </div>

                <div class="col-12 top10">
                  <div class="form-group  input100">
                    <input type="text" name="email" value="<?php Util::imprime($_SESSION[usuario][email]) ?>" class="form-control fundo-form input-lg input100" placeholder="E-MAIL">
                  </div>
                </div>



                <div class="col-6 top10">
                  <div class="form-group  input100">
                    <input type="text" name="tel_residencial"  value="<?php Util::imprime($_SESSION[usuario][tel_residencial]) ?>" class="form-control fundo-form input-lg input100" placeholder="TELEFONE">
                  </div>
                </div>

                <div class="col-6 top10">
                  <div class="form-group  input100">
                    <input type="text" name="tel_celular" value="<?php Util::imprime($_SESSION[usuario][tel_celular]) ?>" class="form-control fundo-form input-lg input100" placeholder="CELULAR">
                  </div>
                </div>

                <div class="col-12 top10">
                  <div class="form-group  input100">
                    <input type="text" name="endereco" value="<?php Util::imprime($_SESSION[usuario][endereco]) ?>" class="form-control fundo-form input-lg input100" placeholder="ENDEREÇO">
                  </div>
                </div>

                <div class="col-5 top10">
                  <div class="form-group  input100">
                    <input type="text" name="numero" value="<?php Util::imprime($_SESSION[usuario][numero]) ?>" class="form-control fundo-form input-lg input100" placeholder="NÚMERO">
                  </div>
                </div>

                <div class="col-7 top10">
                  <div class="form-group  input100">
                    <input type="text" name="complemento" value="<?php Util::imprime($_SESSION[usuario][complemento]) ?>" class="form-control fundo-form input-lg input100" placeholder="COMPLEMENTO">
                  </div>
                </div>

                <div class="col-12 top10">
                  <div class="form-group  input100">
                    <input type="text" name="bairro" value="<?php Util::imprime($_SESSION[usuario][bairro]) ?>" class="form-control fundo-form input-lg input100" placeholder="BAIRRO">
                  </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-7 top10">
                  <div class="form-group  input100">
                    <input type="text" name="cidade" value="<?php Util::imprime($_SESSION[usuario][cidade]) ?>" class="form-control fundo-form input-lg input100" placeholder="CIDADE">
                  </div>
                </div>

                <div class="col-5 top10">
                  <div class="form-group  input100">
                    <input type="text" name="uf" value="<?php Util::imprime($_SESSION[usuario][uf]) ?>" class="form-control fundo-form input-lg input100" placeholder="UF">
                  </div>
                </div>

                <div class="clearfix"></div>



                <div class="col-12 text-right ">
                  <div class="top15 bottom25">
                    <button type="submit" class="btn btn-secondary btn_login" name="btn_atualizar">
                      ATUALIZAR DADOS
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- formulario-->
            <!--  ==============================================================  -->

          </div>



        </div>
      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>



<script>
$(document).ready(function() {
  $('.FormCadastro').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      tel_residencial: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      tel_celular: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      numero: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      uf: {
        validators: {
          notEmpty: {

          }
        }
      },

      senha: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha2',
            message: 'As senhas não sào iguais'
          }
        }
      },
      senha2: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha',
            message: 'As senhas não sào iguais'
          }
        }
      }
    }


    });
  });
  </script>
