<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>LOCAÇÃO DE EQUIPAMENTOS</a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<div class="container bottom40 ">
    <div class="row top15 dicas_titulo">

        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
        <div class="col-6">
            <h2><?php Util::imprime($row[legenda_1]); ?>
                <span><?php Util::imprime($row[legenda_2]); ?></span>
            </h2>
        </div>

        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>


<!-- ======================================================================= -->
<!-- LOCACAO-->
<!-- ======================================================================= -->
<div class="container-fluid bottom50 bg_assistencia">
    <div class="row ">

        <div class="container">
            <div class="row">
                <div class="col-3"></div>


                    <?php

                    //$url2 = Url::getURL(2);
                    $url1 = Url::getURL(1);

                    //  FILTRA AS CATEGORIAS
                    if (isset($url1)) {
                        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
                        $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
                    }

                    //FILTRA PELO TITULO
                    if (isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
                        $complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
                    endif;

                    //  FILTRA PELO TITULO
                    //if (isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto])):
                      //  $complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
                    //endif;

                    //  FILTRA PELO TITULO
                    if (isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos])):
                        $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
                    endif;


                    ?>

                    <?php

                    //  busca os produtos sem filtro
                    $result1 = $obj_site->select("tb_produtos", $complemento);

                    if (mysql_num_rows($result1) == 0):?>




                        <div class='col-12 text-center '>
                            <h2 class='btn_nao_encontrado' style='padding: 20px;'>Nenhum produto encontrado.</h2>

                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-outline-warning btn_detalhe_empresa" data-toggle="modal"
                                    data-target="#exampleModalCenter">
                                FILTRAR POR EQUIPAMENTOS
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="exampleModalLongTitle">EQUIPAMENTOS</h4>
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <!-- ======================================================================= -->
                                            <!-- menu lateral -->
                                            <!-- ======================================================================= -->
                                            <?php

                                            $url2 = Url::getURL(2);

                                            $result = $obj_site->select("tb_categorias_produtos", "and imagem <> ''");
                                            if (mysql_num_rows($result) > 0) {
                                                $i = 0;
                                                while ($row = mysql_fetch_array($result)) {

                                                    ?>

                                                    <div class="co-12 mont menu_lateral">
                                                        <div class="list-group">
                                                            <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos/<?php Util::imprime($row[url_amigavel]); ?>"
                                                               class="list-group-item"
                                                               title="<?php Util::imprime($row1[titulo]); ?>">
                                                                <?php Util::imprime($row[titulo]); ?>
                                                            </a>
                                                            <?php /*
                                $result1 = $obj_site->select("tb_subcategorias_produtos", "and id_categoriaproduto = $row[0] ");
                                if (mysql_num_rows($result1) > 0) {
                                while($row1 = mysql_fetch_array($result1)){
                                ?>
                                <!-- ======================================================================= -->
                                <!-- lista menu    -->
                                <!-- ======================================================================= -->
                                <?php $url2 = Url::getURL(2); ?>
                                <a class="list-group-item <?php if( $url2 == "$row1[url_amigavel]"){ echo 'ativo disabled'; } ?>  list-group-item-action " href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                                <?php Util::imprime($row1[titulo]); ?>
                                </a>
                                <!-- ======================================================================= -->
                                <!-- lista menu    -->
                                <!-- ======================================================================= -->
                                <?php
                              }
                            }
                            */
                                                            ?>
                                                        </div>
                                                    </div>

                                                    <?php

                                                    if ($i == 0) {
                                                        echo '<div class="row"></div>';
                                                        $i = 0;
                                                    } else {
                                                        $i++;
                                                    }

                                                }
                                            }
                                            ?>

                                            <!-- ======================================================================= -->
                                            <!-- menu lateral -->
                                            <!-- ======================================================================= -->

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                SAIR
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                        <?php

                    else:
                        ?>


                        <?php if (isset($url1) >0 ) :?>
                        <div class="col-9 ml-auto total-resultado-busca">
                            <h6 class="font-weight-bold pl-2"><?php echo mysql_num_rows($result1) ?> EQUIPAMENTOS(S) ENCONTRADO(S) .</h6>
                        </div>
                        <div class="col-3"></div>
                        <?php endif; ?>


                        <?php
                        require_once('./includes/lista_equipamentos.php');

                    endif;
                    ?>


            </div>
        </div>

    </div>
</div>
<!-- ======================================================================= -->
<!-- LOCACAO-->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>



