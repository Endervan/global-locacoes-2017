<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>ASSISTÊNCIA TÉCNICA</a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<div class="container">
    <div class="row top15 dicas_titulo">

        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
        <div class="col-6">
            <h2><?php Util::imprime($row[legenda_1]); ?>
                <span><?php Util::imprime($row[legenda_2]); ?></span>
            </h2>
        </div>

        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>


<!-- ======================================================================= -->
<!-- aasistencia tecnica-->
<!-- ======================================================================= -->
<div class="container-fluid">
    <div class="row ">

        <div class="container">
            <div class="row">

                <?php $result = $obj_site->select("tb_servicos");
                require_once('./includes/lista_assistencia_tecnica.php'); ?>
            </div>

        </div>

    </div>
</div>
<!-- ======================================================================= -->
<!-- aasistencia tecnica-->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->
<div class="container top200 bottom75">
    <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
        <div class="col-12 dicas_titulo">
            <h2><?php Util::imprime($row1[legenda_1]); ?> <span><?php Util::imprime($row1[legenda_2]); ?></span></h2>

        </div>

        <?php $result1 = $obj_site->select("tb_produtos", "order by rand() limit 4");
        require_once('./includes/lista_equipamentos.php'); ?>
    </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>



