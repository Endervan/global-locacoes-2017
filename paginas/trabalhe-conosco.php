<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>TRABALHE CONOSCO</a></li>
                </ol>
            </nav>
        </div>
    </div>

</div>


<div class="container">
    <div class="row top15 justify-content-end dicas_titulo">

        <div class="col-6 dicas_titulo">
            <h2><?php Util::imprime($banner[legenda_1]); ?>
                <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
        </div>


        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>


<div class="container-fluid bottom30 top30">
    <div class="row">


        <div class="container ">
            <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
                <div class="row fundo_formulario">


                    <div class="col-4">

                        <ul class=" nav nav-pills nav-fill justify-content-center">


                            <li class="nav-item w-100">
                                <a class=" nav-link"
                                   href="<?php echo Util::caminho_projeto() ?>/contato">FALE CONOSCO</a>
                            </li>
                            <li class="nav-item w-100">
                                <a class="nav-link" href="<?php Util::imprime($config[src_place]); ?>" target="_blank">LOCALIZAÇÃO</a>
                            </li>

                        </ul>

                        <!--  ==============================================================  -->
                        <!-- televendas -->
                        <!--  ==============================================================  -->
                        <?php require_once('./includes/televendas.php') ?>
                        <!--  ==============================================================  -->
                        <!-- televendas -->
                        <!--  ==============================================================  -->

                        <img class="text-center top20"
                             src="<?php echo Util::caminho_projeto() ?>/imgs/fundo_contato.png" alt="">


                    </div>


                    <!--  ==============================================================  -->
                    <!-- FORMULARIO CONTATOS-->
                    <!--  ==============================================================  -->
                    <div class=" col-8">

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="assunto" class="form-control fundo-form"
                                           placeholder="ASSUNTO">
                                    <span class="fas fa-user-circle form-control-feedback"></span>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME">
                                    <span class="fas fa-user form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="email" class="form-control fundo-form"
                                           placeholder="E-MAIL">
                                    <span class="fa fa-envelope form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="tel" name="telefone" class="form-control fundo-form"
                                           placeholder="TELEFONE">
                                    <span class="fa fa-phone form-control-feedback"
                                          data-fa-transform="rotate-90"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="escolaridade" class="form-control fundo-form"
                                           placeholder="ESCOLARIDADE">
                                    <span class="fas fa-graduation-cap form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="area" class="form-control fundo-form" placeholder="ÁREA">
                                    <span class="fas fa-briefcase form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="cargo" class="form-control fundo-form" placeholder="CARGO">
                                    <span class="fas fa-address-book form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="cidade" class="form-control fundo-form"
                                           placeholder="CIDADE">
                                    <span class="fa fa-home form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="estado" class="form-control fundo-form"
                                           placeholder="ESTADO">
                                    <span class="fa fa-globe form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="file" name="curriculo" class="form-control fundo-form"
                                           placeholder="CURRICULO">
                                    <span class="fas fa-file form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <textarea name="mensagem" cols="15" rows="4" class="form-control fundo-form"
                                              placeholder="MENSAGEM"></textarea>
                                    <span class="fas fa-pencil-alt form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 mont text-right padding0 ">
                            <button type="submit" class="btn btn-outline-secondary btn_detalhe_produtos"
                                    name="btn_contato">
                                ENVIAR MENSAGEM
                            </button>
                        </div>


                    </div>


                </div>
            </form>


        </div>
        </form>
        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->


    </div>
</div>

<!-- ======================================================================= -->
<!-- ONDE ESTAMOS   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/mapa.php') ?>
<!-- ======================================================================= -->
<!-- ONDE ESTAMOS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if (isset($_POST[nome])) {

    if (!empty($_FILES[curriculo][name])):
        $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='" . Util::caminho_projeto() . "/uploads/$nome_arquivo' target='_blank'>" . Util::caminho_projeto() . "/uploads/$nome_arquivo</a>";
    endif;

    $texto_mensagem = "
  Assunto: " . $_POST[assunto] . " <br /> <br />

  Nome: " . $_POST[nome] . " <br />
  Email: " . $_POST[email] . " <br />
  Telefone: " . $_POST[telefone] . " <br />
  Escolaridade: " . $_POST[escolaridade] . " <br />
  Area Pretedida: " . $_POST[area] . " <br />
  Cargo Pretedido: " . $_POST[cargo] . " <br />
  Cidade: " . $_POST[cidade] . " <br />
  Estado: " . $_POST[estado] . " <br />
  Mensagem: <br />
  " . nl2br($_POST[mensagem]) . "

  <br><br>
  $texto
  ";


    if (Util::envia_email($config[email_trabalhe_conosco], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email])) {
        Util::envia_email($config[email_copia], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email]);
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
    } else {
        Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
    }

}


?>


<script>
    $(document).ready(function () {
        $('.FormContatos').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-times',
                validating: 'fas fa-refresh'
            },
            fields: {
                nome: {
                    validators: {
                        notEmpty: {
                            message: 'Informe nome.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'insira email.'
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor adicione seu numero.'
                        },
                        phone: {
                            country: 'BR',
                            message: 'Informe um telefone válido.'
                        }
                    },
                },
                assunto: {
                    validators: {
                        notEmpty: {}
                    }
                },
                cidade: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor adicione sua Cidade.'
                        }
                    }
                },
                estado: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor adicione seu Estado.'
                        }
                    }
                },
                escolaridade1: {
                    validators: {
                        notEmpty: {
                            message: 'Sua Informação Acadêmica.'
                        }
                    }
                },
                curriculo: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor insira seu currículo'
                        },
                        file: {
                            extension: 'doc,docx,pdf,rtf',
                            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5 * 1024 * 1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                        }
                    }
                },
                mensagem1: {
                    validators: {
                        notEmpty: {}
                    }
                }
            }
        });
    });


    $('.btn_ligue').popover({
        trigger: 'focus'
    })

</script>
