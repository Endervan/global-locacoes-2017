<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>



    </div>
</div>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[legenda_1]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>EMPRESA</a></li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="container bottom100">
    <div class="row top15 justify-content-end dicas_titulo">


        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>

<div class="container-fluid fundo_empresa">
    <div class="row">
        <div class="container">
            <div class="row">
                <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
                <div class="col-6 dicas_titulo ml-auto">
                    <h2><?php Util::imprime($row[titulo]); ?>
                        <span><?php Util::imprime($row[legenda]); ?></span></h2>

                    <div class="top45">
                        <p><?php Util::imprime($row[descricao]); ?></p>
                    </div>


                    <div class="row top75">
                        <div class="col-6">

                            <!--  ==============================================================  -->
                            <!-- televendas -->
                            <!--  ==============================================================  -->
                            <?php require_once('./includes/televendas.php') ?>
                            <!--  ==============================================================  -->
                            <!-- televendas -->
                            <!--  ==============================================================  -->

                        </div>

                        <div class="col-6">
                            <a href="<?php echo Util::caminho_projeto(); ?>/locacao-de-equipamentos" class="btn btn-block btn_login">
                                COMPRE PELO SITE
                            </a>

                            <img class="top25" src="<?php echo Util::caminho_projeto(); ?>/imgs/pagamentos.png" alt="">
                        </div>
                    </div>


                </div>


                <div class="col-12 bottom50 top105">
                    <div class="linha"></div>
                </div>


            </div>
        </div>
    </div>
</div>

<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<?php require_once('./includes/mapa.php') ?>

<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>
