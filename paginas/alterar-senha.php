<?php

$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if(!$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/locacao-de-equipamentos";
  header("location: $caminho ");

endif;







// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
      <div class="row top240">
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-12 titulo_internas">
              <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
          </div>
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->

          <div class="col-4 top60">
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                              <i class="fas fa-home"></i> HOME
                          </a>
                      </li>
                      <li class="breadcrumb-item active"><a><?php Util::imprime($banner[titulo]); ?></a></li>
                  </ol>
              </nav>
          </div>

          <div class="col-8 top70 dicas_titulo">
              <h2><?php Util::imprime($banner[legenda_1]); ?> <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
          </div>

      </div>
  </div>

  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->
  <div class="container bottom50 top30">
      <div class="row">


          <!-- ======================================================================= -->
          <!-- passo a passo    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/area_clientes.php') ?>
          <!-- ======================================================================= -->
          <!-- passo a passo    -->
          <!-- ======================================================================= -->



          <div class="col-9 titulo_carrinho padding0">

            <!--  ==============================================================  -->
            <!--formulario-->
            <!--  ==============================================================  -->
            <div class="col-12  fundo-formulario">
                <form class="form-inline FormCadastro" role="form" method="post" enctype="multipart/form-data">

              <?php
              if(isset($_POST[btn_cadastrar])):

                $obj_usuario->atualiza_dados($_POST);
                Util::alert_bootstrap("Senha atualizado com sucesso.");

              endif;
              ?>



                <div class="col-12 top10">
                  <div class="form-group input100">
                    <input type="password" name="senha" data-minlength="6" required class="form-control fundo-form1 input100 input-lg" placeholder="CADASTRAR NOVA SENHA">
                  </div>
                </div>

                <div class="col-12 top10">
                  <div class="form-group input100">
                    <input type="password" name="senha2" class="form-control fundo-form1 input100 input-lg" placeholder="CONFIRMAR NOVA SENHA">
                  </div>
                </div>



                <div class="col-12 text-right ">
                  <div class="top15 bottom25">
                    <button type="submit" class="btn btn-outline-secondary btn_detalhe_produtos" name="btn_cadastrar">
                      CADASTRAR
                    </button>
                  </div>
                </div>

              </form>
            </div>
            <!--  ==============================================================  -->
            <!-- formulario-->
            <!--  ==============================================================  -->

          </div>



        </div>
      </div>
    </div>
  </div>


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>



<script>
$(document).ready(function() {
  $('.FormCadastro').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      senha: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha2',
            message: 'As senhas não sào iguais'
          }
        }
      },
      senha2: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha',
            message: 'As senhas não sào iguais'
          }
        }
      }
    }


    });
  });
  </script>
