

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>

<?php


// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() .'/assistencias-tecnicas' );
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">



<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4) ?>
            <h1 class="display-4 ml-5"><?php Util::imprime($row1[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a><?php Util::imprime($row1[titulo]); ?></a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<!-- ======================================================================= -->
<!-- BARRA E TITULO  -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top15 dicas_titulo">

        <div class="col-6">
            <h2><?php Util::imprime($banner[legenda_1]); ?>
                <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
        </div>

        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>
<!-- ======================================================================= -->
<!-- BARRA E TITULO     -->
<!-- ======================================================================= -->



<div class="container top20">
    <div class="row">
        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-6 p-0">

            <!-- ======================================================================= -->
            <!-- slider assistencia    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_produto_servico_dentro.php') ?>
            <!-- ======================================================================= -->
            <!-- slider assistencia    -->
            <!-- ======================================================================= -->

        </div>

        <div class="col-6">
            <div class="descricao_interno">
                <h2><?php Util::imprime($dados_dentro[titulo]); ?></h2>
            </div>
            <div class="top35"><p><?php Util::imprime($dados_dentro[descricao]); ?></p></div>

            <div class="col-12 p-0 top50">

                <a href="#" class="btn disabled btn-block btn-outline-secondary rounded-0 btn_atendimento btn_ligue col-6" >
                    FALE CONOSCO
                </a>

                <div>

                    <div class="row border_pagina descricao_interna top20">

                        <div class="col-6">
                            <h6 class="">COMPRE PELO ATENDIMENTO</h6>
                            <h6><span>ATENDIMENTO</span></h6>
                            <h2><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h2>
                        </div>


                        <?php if (!empty($config[telefone2])): ?>
                            <div class="col-6 top20 ">
                                <h6><span>WHATTSAPP</span> <i class="fab fa-whatsapp right10"></i></h6>
                                <h2> <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h2>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($config[telefone3])): ?>
                            <div class="col-6">
                                <h2> <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?></h2>
                            </div>
                        <?php endif; ?>


                        <?php if (!empty($config[telefone4])): ?>
                            <div class="col-6">
                                <h2> <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?></h2>
                            </div>
                        <?php endif; ?>

                    </div>


                </div>


            </div>

        </div>

        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->



        <div class="col-12 bottom50 top105"><div class="linha"></div></div>

    </div>


</div>


<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->
<div class="container bottom75">
    <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
        <div class="col-12 dicas_titulo">
            <h2><?php Util::imprime($row1[legenda_1]); ?> <span><?php Util::imprime($row1[legenda_2]); ?></span></h2>

        </div>

        <?php $result1 = $obj_site->select("tb_produtos", "order by rand() limit 4");
        require_once('./includes/lista_equipamentos.php'); ?>
    </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
    $(window).load(function () {


        $('#carousel_item').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            itemWidth: 110,
            itemMargin: 0,
            asNavFor: '#slider_item'
        });

        $('#slider_item').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            sync: "#carousel_item"
        });


    });


    $(window).load(function () {
        $('#slider_carousel').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false, /*tira bolinhas*/
            itemWidth: 352,
            itemMargin: 0,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation a")
        });
    });

    $('.btn_ligue').popover({
        trigger: 'focus'
    })

</script>

