<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();

// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/locacao-de-equipamentos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if (isset($_POST[btn_enviar])):

    //  ARAMAZENO O LOCAL DE ENTREGA
    $_SESSION[id_bairro_entrega] = $_POST[bairro];
    $_SESSION[id_cidade] = $_POST[cidade];


    if (!isset($_SESSION[usuario])):
        Util::script_location(Util::caminho_projeto() . "/autenticacao");
    else:
        Util::script_location(Util::caminho_projeto() . "/endereco-entrega");
    endif;


endif;


# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if (isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

    $obj_carrinho->atualiza_itens($_POST['qtd']);
    $_SESSION[id_cidade] = $_POST[cidade];

endif;


# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DA CIDADE
# ==============================================================  #
if (isset($_POST[cidade])):
    $_SESSION[id_cidade] = $_POST[cidade];
endif;


# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DO BAIRRO
# ==============================================================  #
if (isset($_POST[bairro])):
    $_SESSION[id_bairro_entrega] = $_POST[bairro];
endif;


# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if (isset($_GET[action])):

    $action = base64_decode($_GET[action]);
    $id = base64_decode($_GET[id]);

    //  ESCOLHO A OPCAO
    switch ($action):

        case 'del':
            $obj_carrinho->del_item($id);
            break;

    endswitch;

endif;


# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if (isset($_GET[action]) and $_GET[action] == 'add'):

    $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

    <?php require_once('./includes/js_css.php') ?>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#cidade').change(function () {
                $('#bairro').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_taxas.php?id=' + $('#cidade').val());
            });
        });
    </script>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>


<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top240">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a><?php Util::imprime($banner[titulo]); ?></a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<div class="container bottom50 top30">
    <div class="row">

        <!-- ======================================================================= -->
        <!-- passo a passo    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/passo_a_passo.php') ?>
        <!-- ======================================================================= -->
        <!-- passo a passo    -->
        <!-- ======================================================================= -->


        <div class="col-9 titulo_carrinho">
            <div class="dicas_titulo">
                <h2><?php Util::imprime($banner[legenda_1]); ?> <span><?php Util::imprime($banner[legenda_2]); ?></span>
                    <i class="fas fa-shopping-cart ml-3"></i></h2>
            </div>


            <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post" name="form_produto_final"
                  id="form_produto_final">


                <!--  ==============================================================  -->
                <!-- CARRINHO-->
                <!--  ==============================================================  -->
                <div class="tb-lista-itens top30">


                    <?php if (count($_SESSION[produtos]) == 0): ?>
                    <div class="alert alert-danger">
                        <h3 class=''>Nenhum produto foi adicionado ao carrinho.<br></h3>
                    </div>

                    <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos"
                       class="btn btn btn-outline-secondary rounded-0 btn_detalhe_produtos">CONTINUAR COMPRANDO <i
                                class="fa fa-angle-right left10" aria-hidden="true"></i>
                    </a>
                </div>

                <?php else: ?>


                    <table class="table">
                        <tbody>


                        <?php foreach ($_SESSION[produtos] as $key => $dado): ?>
                            <tr>
                                <td>
                                    <?php $obj_site->redimensiona_imagem("../uploads/$dado[imagem]", 50, 50, array('alt' => $dado[titulo])); ?>
                                </td>
                                <td align="left">
                                    <h6 class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></h6>
                                </td>
                                <td class=" text-center">
                                    QTD. <br>
                                    <input type="number" class="input-lista-prod-orcamentos" min="1" name="qtd[]"
                                           value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top"
                                           title="Digite a quantidade desejada">

                                    <input name="idproduto[]" type="hidden"
                                           value="<?php echo $_SESSION[produtos][idproduto]; ?>"/>
                                </td>

                                <td class="text-center">
                                    <div class="text-center">VALOR</div>
                                    <h5 class="top10">R$ <?php echo Util::formata_moeda($dado[preco]) ?></h5>
                                </td>

                                <td class="text-center">
                                    REMOVE <br>

                                    <a class="btn btn-vermelho"
                                       href="?id=<?php echo base64_encode($key) ?>&action=<?php echo base64_encode("del") ?>"
                                       data-toggle="tooltip" data-placement="top" title="Excluir">
                                        <i class="fas fa-times-circle" style="color: #000;"></i>
                                    </a>
                                </td>

                            </tr>
                            <?php $total += $dado[preco] * $dado[qtd]; ?>
                        <?php endforeach; ?>


                        </tbody>
                    </table>


                    <div class="col-12 padding0 carrinho_total">
                        <!--  ==============================================================  -->
                        <!-- CAL CARRINHO -->
                        <!--  ==============================================================  -->
                        <div class="media">

                            <?php if (count($_SESSION[produtos]) > 0): ?>

                                <div class="col-4   top30 text-center">
                                    <input class="btn btn-block btn_atendimento btn-lg" type="submit"
                                           name="btn_atualizar"
                                           id="btn_atualizar" value="ATUALIZAR CARRINHO"/>
                                </div>

                            <?php endif; ?>

                            <div class="col-8 ml-auto">
                                <h6><b>LOCAL DE ENTREGA</b></h6>
                                <select name="cidade" id="cidade" class="input100 top10 form-control btn-cinza">
                                    <option value="" disabled selected>CIDADE</option>
                                    <?php

                                    $result = $obj_site->select("tb_cidades", "order by uf, titulo");
                                    if (mysql_num_rows($result) > 0) {
                                        while ($row = mysql_fetch_array($result)) {
                                            ?>
                                            <option <?php if ($row[idcidade] == $_SESSION[id_cidade]) {
                                                echo 'selected';
                                            } ?> value="<?php Util::imprime($row[idcidade]); ?>"
                                                 data-tokens="<?php Util::imprime($row[uf] . ' - ' . $row[titulo]); ?>"><?php Util::imprime($row[uf] . ' - ' . $row[titulo]); ?></option>
                                            <?php
                                        }
                                    }

                                    ?>

                                </select>


                                <select name="bairro" id="bairro" class="input100 form-control btn-cinza top15"
                                        onchange="this.form.submit()">
                                    <option value="" disabled selected>BAIRRO SETOR</option>
                                    <?php
                                    if (!empty($_SESSION[id_cidade])) {
                                        $result = $obj_site->select("tb_fretes", "and id_cidade = '$_SESSION[id_cidade]' ");
                                        if (mysql_num_rows($result) > 0) {
                                            while ($row = mysql_fetch_array($result)) {
                                                ?>
                                                <option <?php if ($row[idfrete] == $_SESSION[id_bairro_entrega]) {
                                                    echo 'selected';
                                                } ?> value="<?php Util::imprime($row[idfrete]); ?>"
                                                     data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-12">
                            <?php if (isset($_SESSION[produtos]) == 0): ?>
                                <p class="top15 pull-right">
                                    TAXA DE ENTREGA: R$ 0,00</p>
                            <?php else: ?>
                                <p class="top15 pull-right">
                                    TAXA DE ENTREGA:
                                    <?php
                                    if (isset($_SESSION[id_bairro_entrega])):
                                        $frete = $obj_carrinho->get_frete($_SESSION[id_bairro_entrega]);
                                        if ($frete[valor] == 0) {
                                            echo "<span class='text-success'><b>GRÁTIS</b></span>";
                                        } else {
                                            echo 'R$ ' . Util::formata_moeda($frete[valor]);
                                        }

                                    endif;
                                    ?>
                                </p>
                            <?php endif; ?>
                        </div>

                        <div class="clearfix"></div>


                        <p class="col-12 bottom20 text-right">
                            <b>VALOR TOTAL: R$ <?php echo Util::formata_moeda($total + $frete[valor]) ?></b>
                        </p>

                        <div class="clearfix"></div>


                        <div class="row">
                            <div class="col-4 ml-auto">

                                <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos"
                                   class="btn btn-block btn-secondary btn_comprar1">
                                    CONTINUAR NO SITE <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                                </a>

                            </div>

                            <div class="col-4">
                                <?php if (isset($_SESSION[id_bairro_entrega]) and !empty($_SESSION[id_bairro_entrega])): ?>
                                    <i class="fas fa-shopping-cart ml-auto fa-1x" style="border-radius:20px;padding:2px;color: #fff;border: 2px solid #fff;position: absolute;left: 29px;top: 10px;"></i>
                                    <input type="submit" name="btn_enviar" id="btn_enviar"
                                           class="btn btn-block btn-outline-secondary rounded-0 btn_detalhe_produtos  " value="FINALIZAR PEDIDO"/>
                                <?php endif; ?>
                            </div>


                        </div>


                        <input type="hidden" name="action" value="atualiza_qtd"/>

                        <!--  ==============================================================  -->
                        <!-- CAL CARRINHO -->
                        <!--  ==============================================================  -->

                    </div>

                    <!--  ==============================================================  -->
                    <!-- CARRINHO-->
                    <!--  ==============================================================  -->

                <?php endif; ?>


            </form>

        </div>

    </div>
</div>

</div>
<!-- ======================================================================= -->
<!--  DESCRICAO -->
<!-- ======================================================================= -->

<div class="container-fluid pagamentos">
</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>
