<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if (!$obj_usuario->verifica_usuario_logado()):
    $caminho = Util::caminho_projeto() . "/produtos";
    header("location: $caminho");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if (!isset($_SESSION[produtos])):
    $caminho = Util::caminho_projeto() . "/produtos";
    header("location: $caminho");
endif;


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

    <?php require_once('./includes/js_css.php') ?>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#cidade').change(function () {
                $('#bairro').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_taxas.php?id=' + $('#cidade').val());
            });
        });
    </script>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>


<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top240">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a><?php Util::imprime($banner[titulo]); ?></a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<!-- ======================================================================= -->
<!--  DESCRICAO -->
<!-- ======================================================================= -->
<div class="container-fluid bg_fundo_carrinho">
    <div class="row">

        <div class="container bottom50 top30">
            <div class="row">

                <!-- ======================================================================= -->
                <!-- passo a passo    -->
                <!-- ======================================================================= -->
                <?php require_once('./includes/passo_a_passo.php') ?>
                <!-- ======================================================================= -->
                <!-- passo a passo    -->
                <!-- ======================================================================= -->


                <div class="col-9">

                    <?php
                    if (isset($_POST[tipo_pagamento])):

                        # ==============================================================  #
                        # VERIFICO SE FOI ENVIADO A OS DADOS
                        # ==============================================================  #
                        if (isset($_POST[btn_cadastrar])):

                            echo $obj_carrinho->finaliza_venda($_POST[tipo_pagamento]);

                        endif;

                    else:
                        ?>
                        <form name="form_pagamento" id="form_pagamento" class="form_mensagem formulario1" action=""
                              method="post">
                            <div class="dicas_titulo">
                                <h2><?php Util::imprime($banner[legenda_1]); ?>
                                    <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
                            </div>

                            <div class="fundo-form col-12 top10">
                                <label>
                                    <h6>
                                        <input type="radio" name="tipo_pagamento" id="paypal"
                                               class="validate[required] top0" value="paypal">
                                        PayPal</h6>
                                </label>
                            </div>

                            <div class="fundo-form col-12 top10">
                                <label>
                                    <h6>
                                        <input type="radio" name="tipo_pagamento" id="pagseguro"
                                               class=" validate[required] top0" value="pagseguro" checked>
                                        PagSeguro</h6>
                                </label>
                            </div>

                            <?php /*
                <div class="fundo-form col-12 top10">
                  <label>
                      <h6>
                    <input type="radio" name="tipo_pagamento" id="maquineta" class=" validate[required] top0" value="maquineta" checked>
                  Para pagamento no local - Via maquineta de cartão</h6>
                  </label>
                </div>
 */
                            ?>

                            <div class="col-6 top20">
                                <input type="submit" name="btn_cadastrar" id="btn_cadastrar"
                                       class="btn btn-block btn-outline-secondary rounded-0 btn_detalhe_produtos"
                                       value="EFETUAR PAGAMENTO"/>

                            </div>

                        </form>


                        <?php
                    endif;
                    ?>


                </div>

            </div>
        </div>
        <!-- ======================================================================= -->
        <!--  DESCRICAO -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/rodape.php') ?>
        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->


</body>

</html>
