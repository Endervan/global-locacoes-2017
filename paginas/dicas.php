<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5"><?php Util::imprime($banner[titulo]); ?></h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>DICAS</a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<!-- ======================================================================= -->
<!-- DICAS -->
<!-- ======================================================================= -->
<div class="container bottom100">
    <div class="row top15 dicas_titulo">

        <div class="col-6">
            <h2><?php Util::imprime($banner[legenda_1]); ?>
                <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
        </div>

        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


        <?php $result = $obj_site->select("tb_dicas");
        require_once('./includes/lista_dicas.php'); ?>

    </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->
<div class="container bottom75">
    <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
        <div class="col-12 dicas_titulo">
            <h2>NOSSOS <span><?php Util::imprime($row1[legenda_2]); ?></span></h2>

        </div>

        <?php $result1 = $obj_site->select("tb_produtos", "order by rand() limit 4");
        require_once('./includes/lista_equipamentos.php'); ?>
    </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>



