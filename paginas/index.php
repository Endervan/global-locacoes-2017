<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>
</head>
<body>


<div class="container-fluid topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
</div>

<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->
<div class="container-fluid relativo">
    <div class="row">
        <div id="container_banner">
            <div id="content_slider">
                <div id="content-slider-1" class="contentSlider rsDefault">

                    <?php
                    $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
                    if (mysql_num_rows($result) > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <!-- ITEM -->
                            <div>
                                <?php
                                if ($row[url] == '') {
                                    ?>
                                    <img class="rsImg"
                                         src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                         data-rsw="1920" data-rsh="996" alt=""/>
                                    <?php
                                } else {
                                    ?>
                                    <a href="<?php Util::imprime($row[url]) ?>">
                                        <img class="rsImg"
                                             src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"
                                             data-rsw="1920" data-rsh="996" alt=""/>
                                    </a>
                                    <?php
                                }
                                ?>

                            </div>
                            <!-- FIM DO ITEM -->
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- SLIDER    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- PAGE NO CARTAO,COMPRA SEGURA ETC..    -->
<!-- ======================================================================= -->
<div class="container top20">
    <div class="row">

        <div class="col-3">
            <a
               class="btn disabled btn-block  btn_confira" role="button" aria-pressed="true">
                PAGUE NO <span>CARTÃO</span>
            </a>

            <div class="img_confira">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_1.png" alt="">
            </div>
        </div>


        <div class="col-3">
            <a
               class="btn disabled btn-block  btn_confira" role="button" aria-pressed="true">
                COMPRA <span>SEGURA</span>
            </a>

            <div class="img_confira">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_2.png" alt="">
            </div>
        </div>





        <div class="col-3">
            <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos"
               class="btn btn-block  btn_confira" role="button" aria-pressed="true">
                CONFIRA <span>LOCAÇÕES</span>
            </a>

            <div class="img_confira">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_3.png" alt="">
            </div>
        </div>






        <div class="col-3">
            <a href="<?php echo Util::caminho_projeto() ?>/contato" disabled
               class="btn btn-block  btn_confira" role="button" aria-pressed="true">
               FALE <span>CONOSCO</span>
            </a>

            <div class="img_confira">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_4.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- PAGE NO CARTAO,COMPRA SEGURA ETC..    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->
<div class="container top50">
    <div class="row">

        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
        <div class="col-6 top20 bottom40 dicas_titulo ">
            <h2><?php Util::imprime($row[legenda_1]); ?> <span><?php Util::imprime($row[legenda_2]); ?></span>
            </h2>
        </div>

        <div class="col-6 top20">
            <form  action="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos/"  method="post">

                <div class="input-group mb-3">
                    <input type="text" name="busca_produtos" class="form-control btn_buscar border-right-0 rounded-0" placeholder="ENCONTRE EQUIPAMENTO QUE PRECISA" aria-label="BUSCAR PRODUTOS" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary btn_buscar border-left-0 rounded-0"  type="submit">
                            <i class="fas fa-search" ></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-12">
            <div class="border_prod"></div>
        </div>

        <div class="col-10">
            <div class=" lista_categorias">
                <div id="slider_categorias" class="flexslider">
                    <ul class="slides">
                        <?php
                        $i = 0;
                        if ($i == 0) {
                            $active = 'active';
                        } else {
                            $active = 'active1';
                        }
                        $result1 = $obj_site->select("tb_categorias_produtos", "and exibir_home = 'SIM'");
                        if (mysql_num_rows($result1) > 0) {
                            while ($row1 = mysql_fetch_array($result1)) {
                                ?>
                                <li class="text-center ">
                                    <a class="btn btn-block rounded-0 <?php echo $active; ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos/<?php Util::imprime($row1[url_amigavel]); ?>"
                                       title="<?php Util::imprime($row1[titulo]); ?>">
                                        <?php Util::imprime($row1[titulo]); ?>
                                    </a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>


        <div class="col-2">
            <a class="btn btn-outline-secondary btn-block rounded-0 btn_outiline"
               href="<?php echo Util::caminho_projeto() ?>/categorias" title="SAIBA MAIS">
                VER TODAS
            </a>

        </div>

        <div class="col-12">
            <div class="border_prod"></div>

            <div class="seta_cat text-right top10">
                <div class="custom-navigation1">
                    <a href="#" class="flex-prev"><i class="fas fa-chevron-circle-left"></i></a>
                    <!-- <div class="custom-controls-container"></div> -->
                    <a href="#" class="flex-next"><i class="fas fa-chevron-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- PRODUTOS -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row">
        <?php $result1 = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' limit 8");
        require_once('./includes/lista_equipamentos.php'); ?>

        <div class="col-3 top80 ml-auto text-right">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos"
               class="btn btn-block btn-outline-warning btn_detalhe_produtos btn_compra btn_todos" role="button" aria-pressed="true">
                TODOS OS EQUIPAMENTOS
            </a>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- ASSISTENCIA TECNICA -->
<!-- ======================================================================= -->
<div class="container-fluid container_servicos_home">
    <div class="row ">

        <div class="container">
            <div class="row">
                <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
                <div class="col-12 top20 dicas_titulo ">
                    <h2><?php Util::imprime($row[legenda_1]); ?> <span><?php Util::imprime($row[legenda_2]); ?></span>
                    </h2>
                </div>

                <div class="col-3"></div>
                <?php $result = $obj_site->select("tb_servicos", "and exibir_home = 'SIM' limit 3");
                require_once('./includes/lista_assistencia_tecnica.php'); ?>
            </div>
        </div>


    </div>
</div>
<!-- ======================================================================= -->
<!-- ASSISTENCIA TECNICA -->
<!-- ======================================================================= -->


<div class="container-fluid container_empresa_home">
    <div class="row ">
        <!-- ======================================================================= -->
        <!-- EMPRESA HOME -->
        <!-- ======================================================================= -->
        <div class="container">
            <div class="row empresa_home">
                <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                <div class="col-7 top60 text-right">
                    <h2><?php Util::imprime($row1[titulo]); ?></h2>
                </div>
                <div class="col-6 top65 overflow_emp">
                    <p><?php Util::imprime($row1[descricao]); ?></p>
                </div>
                <div class="col-12 top40 text-right">
                    <div class="col-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/empresa"
                           class="btn btn-outline-warning btn_detalhe_empresa" role="button" aria-pressed="true">MAIS
                            DETALHES
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======================================================================= -->
        <!-- EMPRESA HOME -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- DICAS -->
        <!-- ======================================================================= -->
        <div class="container bottom50 top30">
            <div class="row">
                <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
                <div class="col-12 dicas_titulo">
                    <div class="col-7 top65">
                        <h2><?php Util::imprime($row[legenda_1]); ?>
                            <span><?php Util::imprime($row[legenda_2]); ?></span></h2>
                    </div>

                </div>

                <?php $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
                require_once('./includes/lista_dicas.php'); ?>

            </div>
        </div>
        <!-- ======================================================================= -->
        <!-- DICAS    -->
        <!-- ======================================================================= -->

    </div>
</div>

<!-- ======================================================================= -->
<!-- rodape   =====================================================-->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">


<script>
    jQuery(document).ready(function ($) {
        // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
        // it's recommended to disable them when using autoHeight module
        $('#content-slider-1').royalSlider({
            autoHeight: true,
            arrowsNav: true,
            arrowsNavAutoHide: false,
            keyboardNavEnabled: true,
            controlNavigationSpacing: 0,
            controlNavigation: 'tabs',
            autoScaleSlider: false,
            arrowsNavAutohide: true,
            arrowsNavHideOnTouch: true,
            imageScaleMode: 'none',
            globalCaption: true,
            imageAlignCenter: false,
            fadeinLoadedSlide: true,
            loop: false,
            loopRewind: true,
            numImagesToPreload: 6,
            keyboardNavEnabled: true,
            usePreloader: false,
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                pauseOnHover: true,
                delay: 9000
            }

        });
    });
</script>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
    <?php /*
    $(window).load(function () {
        $('#slider_carousel').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas
            animationLoop: true,
            itemWidth: 195,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20

        });
    });

    */ ?>

    $(window).load(function () {
        $('#slider_categorias').flexslider({
            animation: "slide",
            controlNav: false, /*tira bolinhas*/
            animationLoop: true,
            itemWidth: 240,
            itemMargin: 0,
            inItems: 2,
            maxItems: 20,
            controlsContainer: $(".custom-controls-container"),
            customDirectionNav: $(".custom-navigation1 a")

        });
    });


</script>

