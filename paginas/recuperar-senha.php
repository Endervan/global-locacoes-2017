<?php

$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/autenticacao" );

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 15);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px  center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container top240">
      <div class="row">
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-12 titulo_internas">
              <div class="ml-5">
                  <h1 class="display-4"><?php Util::imprime($banner[legenda_1]); ?></h1>
                  <h3><?php Util::imprime($banner[legenda_2]); ?></h3>
              </div>
          </div>
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->

          <div class="col-12 top60">
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                              <i class="fas fa-home"></i> HOME
                          </a>
                      </li>
                      <li class="breadcrumb-item active"><a>RECUPERAR</a></li>
                  </ol>
              </nav>
          </div>

      </div>
  </div>



      <div class="container bottom50 top30">
        <div class="row">



          <div class="col-12 titulo_carrinho">

            <h3>ADICIONE SEU EMAIL CADASTRADO PARA RECEBER NOVA SENHA</h3>
              <form class="form-inline FormLogin pt30"  role="form"  method="post" enctype="multipart/form-data">

            <!--  ==============================================================  -->
            <!-- RESET SENHA->
            <!--  ==============================================================  -->
            <?php
            if(isset($_POST['btn_logar'])){
              if($obj_usuario->recupera_senha($_POST['email']) == true){
                Util::alert_bootstrap("
                <h5 class='text-success'>Sua nova senha foi enviado para o e-mail cadastrado.</h5>
                <br><br>
                <a href='".Util::caminho_projeto()."/autenticacao' title=''><h3>Clique aqui para efetuar login</h3></a>
                "); // se nao retornou mostra isso
                //Util::script_location(Util::caminho_projeto() . "/autenticacao");
                //header("location: index.php");
              }else{
                Util::alert_bootstrap("<p class='text-danger'>Não foi possível recuperar sua senha. E-mail não encontrado.</p>");
              }
            }
            ?>


                <div class="col-8 fundo-formulario">
                  <div class="form-group input100" >
                    <input type="text"  id="btn_logar" name="email"  class="form-control fundo-form1 input-lg w-100" placeholder="E-MAIL" required>
                  </div>
                </div>


                <div class="col-3 top25">
                  <div class=" bottom25">
                    <button type="submit" class="btn btn-block btn-outline-secondary btn_detalhe_produtos btn_compra" id="btn_logar" name="btn_logar">
                      ENVIAR
                    </button>
                  </div>
                </div>

              </form>
            <!--  ==============================================================  -->
            <!-- RESET SENHA->
            <!--  ==============================================================  -->


        </div>
      </div>
    </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>



<script>
$(document).ready(function() {
  $('.FormLogin').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      email: {
        validators: {
          notEmpty: {
            message: 'Adicione email Cadastrado'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },


    }
  });
});
</script>
