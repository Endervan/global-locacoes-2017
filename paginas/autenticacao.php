<?php
$obj_usuario = new Usuario();

//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/endereco-entrega" );

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <?php require_once('./includes/js_css.php') ?>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
}
</style>




<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <div class="container">
      <div class="row top240">
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->
          <div class="col-12 titulo_internas">
              <h1 class="display-4 ml-5"><?php Util::imprime($banner[legenda_1]); ?></h1>
          </div>
          <!-- ======================================================================= -->
          <!-- TITULO GERAL    -->
          <!-- ======================================================================= -->

          <div class="col-12 top60">
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                          <a href="<?php echo Util::caminho_projeto() ?>/">
                              <i class="fas fa-home"></i> HOME
                          </a>
                      </li>
                      <li class="breadcrumb-item active"><a><?php Util::imprime($banner[legenda_1]); ?></a></li>
                  </ol>
              </nav>
          </div>

      </div>
  </div>


  <!-- ======================================================================= -->
  <!--  DESCRICAO -->
  <!-- ======================================================================= -->


      <div class="container bottom50 top20">
        <div class="row">

          <div class="col-3 text-center lista-passos-carrinho">
            <div class="list-group">
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled ">  MEU CARRINHO <i class="fas fa-shopping-cart "></i></a>
              <a href="javacript:void(0);" class="list-group-item btn-lg active">  CADASTRO <i class="fas fa-user"></i></i></a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> PAGAMENTO <i class="far fa-credit-card"></i> </a>
              <a href="javacript:void(0);" class="list-group-item btn-lg disabled"> FINALIZADO  <i class="fas fa-check"></i></a>
            </div>
          </div>



          <div class="col-4">

            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->
              <div class="dicas_titulo ">


                    <h2>JÁ SOU <span>CADASTRADO</span></h2>


              </div>

              <?php
              # ==============================================================  #
              # VERIFICO SE E PARA EFETUAR O LOGIN
              # ==============================================================  #
              if(isset($_POST[btn_logar])):

                if($obj_usuario->verifica_usuario($_POST[email], $_POST[senha])):

                  if (isset($_SESSION[produtos])) {
                    Util::script_location(Util::caminho_projeto()."/endereco-entrega");
                  }else{
                    Util::script_location(Util::caminho_projeto()."/locacao-de-equipamentos");
                  }

                else:
                  Util::alert_bootstrap("Usuário ou senha inválido.");
                endif;

              endif;
              ?>

              <div class="fundo-formulario">
                <form class="form-inline FormLogin " role="form" method="post" enctype="multipart/form-data">

                  <div class="col-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    </div>
                  </div>

                  <div class="col-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">
                    </div>
                  </div>


                  <div class="col-6 ">
                    <a class="btn btn-lg btn_esqueceu" href="<?php echo Util::caminho_projeto() ?>/recuperar-senha" title="Esqueci minha senha">
                    Esqueciu senha ?
                    </a>
                  </div>

                  <div class="col-6 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn-block btn-outline-secondary btn_detalhe_produtos" name="btn_logar">
                        ENTRAR
                      </button>
                    </div>
                  </div>
                </form>
              </div>
          </div>
            <!--  ==============================================================  -->
            <!-- SOU CADASTRADO-->
            <!--  ==============================================================  -->



            <!--  ==============================================================  -->
            <!-- NAO SOU CADASTRADO-->
            <!--  ==============================================================  -->
            <div class="col-5 p-0">
              <div class="dicas_titulo">
                    <h2>CRIA NOVA CONTA</h2>

              </div>

              <div class="fundo-formulario">
                <?php
                # ==============================================================  #
                # VERIFICO SE E PARA CADASTRAR O USUARIO
                # ==============================================================  #
                if(isset($_POST[btn_cadastrar])):

                  if($obj_usuario->cadastra_usuario($_POST) != false)
                  {
                    Util::script_location(Util::caminho_projeto() . "/endereco-entrega");
                  }
                  else
                  {
                    Util::alert_bootstrap("Esse email já está cadastrado");
                  }

                endif;
                ?>
                <form class="form-inline FormCadastro pt30" role="form" method="post" enctype="multipart/form-data">

                  <div class="col-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="nome" class="form-control fundo-form1 input-lg input100" placeholder="NOME">
                    </div>
                  </div>

                  <div class="col-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                    </div>
                  </div>



                  <div class="col-6 top10">
                    <div class="form-group  input100">
                      <input type="text" name="tel_residencial" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                    </div>
                  </div>

                  <div class="col-6 top10">
                    <div class="form-group  input100">
                      <input type="text" name="tel_celular" class="form-control fundo-form1 input-lg input100" placeholder="CELULAR">
                    </div>
                  </div>

                  <div class="col-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="endereco" class="form-control fundo-form1 input-lg input100" placeholder="ENDEREÇO">
                    </div>
                  </div>

                  <div class="col-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="numero" class="form-control fundo-form1 input-lg input100" placeholder="NÚMERO">
                    </div>
                  </div>

                  <div class="col-7 top10">
                    <div class="form-group  input100">
                      <input type="text" name="complemento" class="form-control fundo-form1 input-lg input100" placeholder="COMPLEMENTO">
                    </div>
                  </div>

                  <div class="col-12 top10">
                    <div class="form-group  input100">
                      <input type="text" name="bairro" class="form-control fundo-form1 input-lg input100" placeholder="BAIRRO">
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-7 top10">
                    <div class="form-group  input100">
                      <input type="text" name="cidade" class="form-control fundo-form1 input-lg input100" placeholder="CIDADE">
                    </div>
                  </div>

                  <div class="col-5 top10">
                    <div class="form-group  input100">
                      <input type="text" name="uf" class="form-control fundo-form1 input-lg input100" placeholder="UF">
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha" class="form-control fundo-form1 input100 input-lg" placeholder="SENHA">
                    </div>
                  </div>

                  <div class="col-12 top10">
                    <div class="form-group input100">
                      <input type="password" name="senha2" class="form-control fundo-form1 input100 input-lg" placeholder="CONFIRMAR SENHA">
                    </div>
                  </div>

                  <div class="col-12 text-right">
                    <div class="top15 bottom25">
                      <button type="submit" class="btn btn-outline-secondary btn_detalhe_produtos" name="btn_cadastrar">
                        CADASTRAR
                      </button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
            <!--  ==============================================================  -->
            <!-- NAO SOU CADASTRADO-->
            <!--  ==============================================================  -->







          </div>
        </div>
        <!-- ======================================================================= -->
        <!--  DESCRICAO -->
        <!-- ======================================================================= -->


      </div>














  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(document).ready(function() {
  $('.FormLogin').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {

      senha: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },



    }
  });
});
</script>


<script>
$(document).ready(function() {
  $('.FormCadastro').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      tel_residencial: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      tel_celular: {
        validators: {
          notEmpty: {

          },
          phone: {
            country: 'BR',
            message: 'Telefone inválido'
          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      numero: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      uf: {
        validators: {
          notEmpty: {

          }
        }
      },

      senha: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha2',
            message: 'As senhas não sào iguais'
          }
        }
      },
      senha2: {
        validators: {
          notEmpty: {

          },
          identical: {
            field: 'senha',
            message: 'As senhas não sào iguais'
          }
        }
      }

    }
  });
});
</script>
