<?php
// INTERNA
$url = Url::getURL(1);


if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 125px center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<div class="container">
    <div class="row top50">
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-12 titulo_internas">
            <h1 class="display-4 ml-5">DICAS</h1>
        </div>
        <!-- ======================================================================= -->
        <!-- TITULO GERAL    -->
        <!-- ======================================================================= -->

        <div class="col-12 top60">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                            <i class="fas fa-home"></i> HOME
                        </a>
                    </li>
                    <li class="breadcrumb-item active"><a>DICAS</a></li>
                </ol>
            </nav>
        </div>

    </div>
</div>


<!-- ======================================================================= -->
<!-- BARRA E TITULO DICAS -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row top15 dicas_titulo">

        <div class="col-6">
            <h2><?php Util::imprime($banner[legenda_1]); ?>
                <span><?php Util::imprime($banner[legenda_2]); ?></span></h2>
        </div>

        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/barra_pesquisa.php') ?>
        <!-- ======================================================================= -->
        <!-- barra pesquisa    -->
        <!-- ======================================================================= -->


    </div>
</div>
<!-- ======================================================================= -->
<!-- BARRA E TITULO DICAS    -->
<!-- ======================================================================= -->


<div class="container top40">
    <div class="row">
        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->
        <div class="col-9 descricao_interna">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 848, 255, array("class" => "w-100 bottom20", "alt" => "$row[titulo]")) ?>
            <div class="top20">
                <h2><?php Util::imprime($dados_dentro[titulo]); ?></h2>
            </div>
            <div class=" top30"><p><?php Util::imprime($dados_dentro[descricao]); ?></p></div>
        </div>
        <!-- ======================================================================= -->
        <!-- DESCRICAO GERAL    -->
        <!-- ======================================================================= -->


        <div class="col-3">
            <!-- ======================================================================= -->
            <!-- OUTRAS DICAS    -->
            <!-- ======================================================================= -->
            <?php $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
            $i = 0;
            if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                    ?>

                    <div class="col-12 dicas">
                        <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"
                           title="<?php Util::imprime($row[titulo]); ?>">

                            <div class="card">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 195, array("class" => "card-img-top", "alt" => "$row[titulo]")) ?>

                                <div class="card-body top10">
                                    <div class=" dica_line_titulo">
                                        <h6><?php Util::imprime($row[titulo]); ?></h6>
                                    </div>


                                </div>
                            </div>
                        </a>
                        <div class="col-12 bottom20 top10">
                            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"
                               class="btn btn_detalhe_dicas " role="button" aria-pressed="true">
                                <div class="media">
                                    <i class="fas fa-plus-circle mr-3 fa-2x"></i>
                                    <div class="media-body align-self-center">
                                        LEIA MAIS
                                    </div>
                                </div>
                            </a>


                        </div>

                    </div>

                    <?php

                }
            }
            ?>

        <!-- ======================================================================= -->
        <!-- OUTRAS DICAS    -->
        <!-- ======================================================================= -->



        <div class="col-12 p-0 top55">

            <a href="<?php echo Util::caminho_projeto() ?>/contato" class="btn btn-block btn-outline-secondary rounded-0 btn_atendimento btn_ligue">
                FALE CONOSCO
            </a>

            <div >

                <div class="row border_pagina descricao_interna top20">

                    <div class="col-12">
                        <h6 class="">COMPRE PELO ATENDIMENTO</h6>
                        <h6><span>ATENDIMENTO</span></h6>
                        <h2><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h2>
                    </div>


                    <?php if (!empty($config[telefone2])): ?>
                        <div class="col-12 top20 ">
                            <h6><span>WHATTSAPP</span> <i class="fab fa-whatsapp right10"></i></h6>
                            <h2> <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h2>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($config[telefone3])): ?>
                        <div class="col-12">
                            <h2> <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?></h2>
                        </div>
                    <?php endif; ?>


                    <?php if (!empty($config[telefone4])): ?>
                        <div class="col-12">
                            <h2> <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?></h2>
                        </div>
                    <?php endif; ?>

                </div>


            </div>


        </div>

        </div>

        <div class="col-12 bottom50 top105"><div class="linha"></div></div>

    </div>


</div>


<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->
<div class="container bottom75">
    <div class="row">
        <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
        <div class="col-12 dicas_titulo">
            <h2>NOSSOS <span><?php Util::imprime($row1[legenda_2]); ?></span></h2>

        </div>

        <?php $result1 = $obj_site->select("tb_produtos", "order by rand() limit 4");
        require_once('./includes/lista_equipamentos.php'); ?>
    </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>

