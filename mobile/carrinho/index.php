<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_carrinho = new Carrinho();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if (isset($_GET[action]) and $_GET[action] == 'add'):

    $obj_carrinho->add_item($_GET['idproduto']);

endif;


# ==============================================================  #
# VERIFICO SE PARA FINALIZAR O CARRINHO, VERIFICA SE O USUARIO ESTA LOGADO
# ==============================================================  #
if (isset($_GET[btn_enviar])):

    if (!isset($_SESSION[usuario])):
        Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
    else:
        Util::script_location(Util::caminho_projeto() . "/mobile/endereco-entrega");
    endif;

endif;


# ==============================================================  #
# VERIFICO SE E PARA ATUALIZAR O CARRINHO
# ==============================================================  #
if (isset($_GET[btn_atualizar])):

    $obj_carrinho->atualiza_itens($_GET['qtd']);


endif;


# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DA CIDADE
# ==============================================================  #
if (isset($_GET[cidade])):
    $_SESSION[id_cidade] = $_GET[cidade];
endif;


# ==============================================================  #
# VERIFICO SE E PARA ARMAZENAR O ID DO BAIRRO
# ==============================================================  #
if (isset($_GET[bairro])):
    $_SESSION[id_bairro_entrega] = $_GET[bairro];
endif;


# ==============================================================  #
# VERIFICO SE E PARA LIMPAR A SESSAO DA ENTREGA
# ==============================================================  #
if (isset($_GET[alterarlocal]) and $_GET[alterarlocal] = 'sim'):
    unset($_SESSION[id_cidade]);
    unset($_SESSION[id_bairro_entrega]);
endif;


# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if (isset($_GET[action])):

    $action = ($_GET[action]);
    $id = ($_GET[id]);

    //  ESCOLHO A OPCAO
    switch ($action):

        case 'del':
            $obj_carrinho->del_item($id);
            break;


    endswitch;

endif;


?>
<!doctype html>
<html amp lang="pt-br">
<head>


    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 25); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 113px;
        }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body>
<?php
$voltar_para = 'locacao-de-equipamentos'; // link de volta, exemplo produtos, dicas, servicos etc
$exibir_link = 'produtos';
require_once("../includes/topo.php");

?>


<div class="row  bg_amarelo top15">
    <div class="col-2 text-center pt10">
        <i class="fas fa-shopping-cart fa-2x" style="color: #fff;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
    </div>
</div>


<?php

//  VERIFICO SE A CIDADE FOI SELECIONADA
if (!isset($_SESSION[id_cidade]) or empty($_SESSION[id_cidade])) {
    exibe_cidades();
}


//  VERIFICO SE O BAIRRO FOI SELECIONADO
if (!isset($_SESSION[id_bairro_entrega]) or empty($_SESSION[id_bairro_entrega])) {
    exibe_bairros();
}

// EXIBE O CARRINHO
if (isset($_SESSION[id_cidade]) and isset($_SESSION[id_bairro_entrega])) {
    exibe_pagina_carrinho();
}
?>


<!--  ==============================================================  -->
<!--  cidades para entrega -->
<!--  ==============================================================  -->
<?php
function exibe_cidades()
{
    $obj_site = new Site();
    ?>
    <div class="row">
        <div class="col-12 menu_Cat top10">


            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active ">
                    ESCOLHA O LOCAL DA ENTREGA
                </a>

                <?php
                $sql = "
          SELECT
          tc.*
          FROM
          tb_cidades tc, tb_fretes tf
          WHERE
          tc.idcidade = tf.id_cidade
          AND tc.ativo = 'SIM'
          AND tf.ativo = 'SIM'
          GROUP BY tc.idcidade
          ORDER BY tc.titulo
          ";
                $result = $obj_site->executaSQL($sql);
                if (mysql_num_rows($result) > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <a class="list-group-item list-group-item-action" href="?cidade=<?php echo $row[idcidade] ?>">
                            <h5 class="text-uppercase"><?php Util::imprime($row[titulo]) ?></h5>
                        </a>

                        <?php
                    }
                }


                ?>

            </div>


        </div>
    </div>
    <?php
}

?>
<!--  ==============================================================  -->
<!--  cidades para entrega -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--  cidades para entrega -->
<!--  ==============================================================  -->
<?php
function exibe_bairros()
{
    $obj_site = new Site();
    ?>
    <div class="row">
        <div class="col-12 menu_Cat top10">


            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action active ">
                    SELECIONE BAIRRO ...
                </a>

                <?php
                $result = $obj_site->select("tb_fretes", "and id_cidade = '$_SESSION[id_cidade]' ");
                if (mysql_num_rows($result) > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <a class="list-group-item list-group-item-action" class=" btn btn_orcamento"
                           href="?cidade=<?php echo $_SESSION[id_cidade]; ?>&bairro=<?php echo $row[idfrete] ?>"><br>
                            <h5 class="text-uppercase"><?php Util::imprime($row[titulo]) ?></h5>
                        </a>


                        <?php
                    }
                }

                ?>

            </div>

        </div>
    </div>
    <?php
}

?>
<!--  ==============================================================  -->
<!--  cidades para entrega -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--  PAGINA DO CARRINHO -->
<!--  ==============================================================  -->
<?php
function exibe_pagina_carrinho()
{
    $obj_site = new Site();
    $obj_carrinho = new Carrinho();
    ?>


    <form method="get" action="#" target="_top">
        <div class="row">

        <!--  ==============================================================  -->
        <!-- CARRINHO-->
        <!--  ==============================================================  -->
        <div class="col-12 tb-lista-itens top10">


            <?php if (count($_SESSION[produtos]) == 0): ?>

            <div class="alert alert-danger text-center top10">
                <h6 class=''>Nenhum produto foi adicionado ao carrinho.<br></h6>
            </div>

            <div class="text-center top10 bottom10">
                <a class="btn btn_orcamento btn_verde"
                   href="<?php echo Util::caminho_projeto() ?>/mobile/locacao-de-equipamentos/">CONTINUAR COMPRANDO</a>
            </div>


        </div>

        <?php else: ?>


            <div class="row">
                <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 24); ?>
                <div class="col-12  top25 empresa_geral">
                    <h6><?php Util::imprime($banner[legenda_1]); ?>
                        <span><?php Util::imprime($banner[legenda_2]); ?></span></h6>
                </div>
            </div>


            <table class="table top15">
                <tbody>

                <tr class="tabela_titulo">
                    <td><span>PRODUTO</span></td>
                    <td></td>
                    <td class="text-center"><span>QDT.</span></td>
                    <td><span>VALOR</span></td>

                </tr>


                <?php foreach ($_SESSION[produtos] as $key => $dado): ?>

                    <tr class=" top20">

                        <td class="text-center" style="padding: 0;">
                            <amp-img
                                    height="50" width="50"
                                    src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dado[imagem]); ?>"
                                    alt="<?php echo Util::imprime($dado[titulo]) ?>">
                            </amp-img>
                            <br>

                            <a class="btn btn-outline-secondary btn_detalhe_produtos"
                               href="?id=<?php echo($key) ?>&action=<?php echo("del") ?>" data-toggle="tooltip"
                               data-placement="top" title="Excluir">
                                Excluir
                            </a>
                        </td>
                        <td align="left">
                            <h6 class="text-uppercase"><?php Util::imprime($dado[titulo]) ?></h6>
                        </td>


                        <td class="text-center">
                            <input type="number" min="1" class="input-lista-prod-orcamentos" name="qtd[]"
                                   value="<?php echo $dado[qtd] ?>" data-toggle="tooltip" data-placement="top"
                                   title="Digite a quantidade desejada">
                            <input name="idproduto[]" type="hidden"
                                   value="<?php echo $_SESSION[produtos][idproduto]; ?>"/>
                        </td>

                        <td class="padding0 col-5" align="left">
                            <h6>R$ <?php echo Util::formata_moeda($dado[preco]) ?></h6>
                        </td>

                    </tr>

                    <?php $total += $dado[preco] * $dado[qtd]; ?>
                <?php endforeach; ?>


                </tbody>
            </table>

        <?php endif; ?>


        <?php if (count($_SESSION[produtos]) > 0): ?>
            <div class="col-12 text-right">
                <input class="btn btn_orcamento btn_verde" type="submit" name="btn_atualizar" id="btn_atualizar"
                       value="ATUALIZAR CARRINHO"/>
            </div>
        <?php endif; ?>


        <!--  ==============================================================  -->
        <!-- CARRINHO-->
        <!--  ==============================================================  -->


        <div class="col-6 local top15">
            <h6>LOCAL DE ENTREGA </h6>
        </div>

        <div class="col-6 text-right top5">
            <a class="btn btn_orcamento btn_verde pull-right" href="?alterarlocal=sim">Alterar Local</a>
        </div>


        <div class="col-12">
            <p class="btn btn_cidade col-12"><?php Util::imprime(Util::troca_value_nome($_SESSION[id_cidade], "tb_cidades", "idcidade", "titulo")); ?></p>
        </div>

        <div class="col-12">
            <p class="btn btn_cidade col-12"><?php Util::imprime(Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo")); ?></p>
        </div>

        <div class="col-12 top15 text-right">
            TAXA DE ENTREGA :
            <?php
            if (isset($_SESSION[id_bairro_entrega])):
                $frete = $obj_carrinho->get_frete($_SESSION[id_bairro_entrega]);
                if ($frete[valor] == 0) {
                    echo "<span class='text-success'><b>GRÁTIS</b></span>";
                } else {
                    echo ' R$' . Util::formata_moeda($frete[valor]);
                }

            endif;
            ?>
        </div>


        <div class="col-12  valor top5 text-right">
            <h5><b> VALOR TOTAL :
                    R$ <?php echo Util::formata_moeda($total + $frete[valor]) ?></b>
            </h5>

            <div class="clearfix"></div>


        </div>

        <div class="col-12 padding0 top30">
            <?php if (isset($_SESSION[id_bairro_entrega]) and !empty($_SESSION[id_bairro_entrega]) and count($_SESSION[produtos]) > 0): ?>
                <i class="fas fa-shopping-cart ml-auto fa-1x"
                   style="border-radius:20px;padding:2px;color: #fff;border: 2px solid #fff;position: absolute;left: 29px;top: 10px;"></i>

                <input type="submit" name="btn_enviar" id="btn_enviar"
                       class="btn btn-lg btn-block btn-outline-secondary btn_detalhe_produtos"
                       value="FINALIZAR PEDIDO"/>
            <?php endif; ?>
        </div>


        <!--  ==============================================================  -->
        <!-- CAL CARRINHO -->
        <!--  ==============================================================  -->
        </div>
        </div>

    </form>

    <div class="row">
        <div class="col-12">
            <a class="btn btn-block btn_continue top20"
               href="<?php echo Util::caminho_projeto(); ?>/mobile/locacao-de-equipamentos">
                CONTINUAR NO SITE
            </a>
        </div>

    </div>


    <?php
}

?>
<!--  ==============================================================  -->
<!--  PAGINA DO CARRINHO -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
