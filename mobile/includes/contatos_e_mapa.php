
<div class="row">
    <div class="col-12 top40">



        <?php
        $ids = explode("/", $_GET[get1]);

        if ($_GET[get1]):?>

        <a class="btn btn-lg btn-block btn_topo btn_ligue top5"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            LIGUE AGORA
        </a>

        <?php else:?>

            <a class="btn btn-lg btn-block btn_topo" href="<?php echo Util::caminho_projeto() ?>/mobile/categoria">
                CONFIRA NOSSOS PRODUTOS <i class="fas fa-caret-down left10"></i>
            </a>

        <?php endif;?>

    </div>

    <div class="col-12 top5">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/formas.png"
                 width="380"
                 height="107"
                 layout="responsive"
                 alt="AMP">
        </amp-img>
    </div>

    <div class="col-12 top20 contato_emp">
        <h6>COMPRE TAMBÉM PELO TELEFONE</h6>

        <div class="col-6 top15 media">
            <i class="fas fa-phone fa-lg align-self-center mr-1" data-fa-transform="rotate-90"></i>
            <div class="media-body">
                <h6 class="mt-0"><span>Atendimento</span></h6>
                <h6><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
            </div>
        </div>

        <div class="col-6 top15 media">
            <i class="fab fa-whatsapp fa-lg align-self-center mr-1"></i>
            <div class="media-body">
                <h6 class="mt-0"><span>Whatsapp</span></h6>
                <h6><?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
            </div>
        </div>


    </div>
</div>


<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row bottom40 top35">
    <div class="col-12 padding0">
        <amp-iframe
            width="480"
            height="300"
            layout="responsive"
            sandbox="allow-scripts allow-same-origin allow-popups"
            frameborder="0"
            src="<?php Util::imprime($config[src_place]); ?>">
        </amp-iframe>

    </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
