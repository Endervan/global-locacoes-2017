

<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if(isset($_POST[btn_enviar])):

    //  ARAMAZENO O LOCAL DE ENTREGA
    $_SESSION[id_bairro_entrega] = $_POST[bairro];
    $_SESSION[id_cidade] = $_POST[cidade];


    if(!isset($_SESSION[usuario])):
        Util::script_location(Util::caminho_projeto() . "/autenticacao");
    else:
        Util::script_location(Util::caminho_projeto()."/endereco-entrega");
    endif;


endif;



# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if(isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

    $obj_carrinho->atualiza_itens($_POST['qtd']);
    $_SESSION[id_cidade] = $_POST[cidade];

endif;






# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if(isset($_GET[action])):

    $action = base64_decode($_GET[action]);
    $id = base64_decode($_GET[id]);

    //  ESCOLHO A OPCAO
    switch($action):

        case 'del':
            $obj_carrinho->del_item($id);
            break;

    endswitch;

endif;



# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if(isset($_GET[action]) and $_GET[action] = 'add'):

    $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>



<div class="row bg_topo">

    <?php
    if(empty($voltar_para)){
        $link_topo = Util::caminho_projeto()."/mobile/";
    }else{
        $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
    }
    ?>



    <div class="col-2 top15">
        <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>

    </div>


    <div class="col-2 text-center top5">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile">
            <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo-paginas.png   "
                     alt="Home" height="50" width="50"
            class="input100">

            </amp-img>
        </a>
    </div>

    <!-- ======================================================================= -->
    <!-- BARRA PESQUISA -->
    <!-- ======================================================================= -->
    <div class="col-4 p-0 top15 barra_pesquisa">
        <form action="<?php echo Util::caminho_projeto() ?>/mobile/locacao-de-equipamentos/" method="get" target = "_top">

            <div class="input-group mb-3">
                <input type="text" name="busca_produtos"
                       class="form-control btn_buscar border-right-0 rounded-0"
                       placeholder="PESQUISA" aria-label="PESQUISA"
                       aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary btn_buscar border-left-0 rounded-0" type="submit">
                        <i class="fas  fa-search fa-lg"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>


    <div class="col-2 pt15 text-center icon">

        <a href="<?php echo Util::caminho_projeto() ?>/mobile/carrinho">
            <i class="fas fa-shopping-cart fa-lg"></i>
        </a>

    </div>

    <div class="col-2 pt15 text-center">
        <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
    </div>






</div>

<div class="row top15">
    <div class="col-6">
        <a class="btn btn-block btn_cat" href="<?php echo Util::caminho_projeto() ?>/mobile/categoria">
            CATEGORIAS <i class="fas fa-caret-down left10"></i>
        </a>

    </div>

    <div class="col-6">
        <a class="btn btn-block btn_topo"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
            LIGUE AGORA
        </a>
    </div>

</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
    <ul class="menu-mobile">
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fas fa-home" aria-hidden="true"></i> Home</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa"> <i class="fas fa-building"></i> A Empresa</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/locacao-de-equipamentos"> <i class="fab fa-product-hunt"></i>  Locação de Equipamentos</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/assistencias-tecnicas"> <i class="fas fa-truck-loading"></i> Assistência Técnica</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas"> <i class="far fa-thumbs-up"></i> Dicas</a></li>
        <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/contato"> <i class="fa fa-envelope" aria-hidden="true"></i> Contato</a></li>
    </ul>



    <div class="top20">
        <?php if (!isset($_SESSION[usuario])): ?>
            <a id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                <i class="far fa-user"></i>   ACESSAR CONTA
            </a>
            <ul class="menu-mobile" aria-labelledby="dLabel-topo">
                <li><a  href="<?php echo Util::caminho_projeto() ?>/mobile/autenticacao"><i class="fas fa-sign-in-alt right10"></i> Entrar</a></li>
            </ul>

        <?php else: ?>

        <a class="btn usuario_topo text-center text-capitalize" id="dLabel-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="far fa-user"></i> <i class="fas fa-sign-in-alt right10"></i> <?php Util::imprime($_SESSION[usuario][nome],25) ?>
        </a>
        <ul class="menu-mobile">
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/meus-pedidos"><i class="fas fa-edit"></i> Meus pedidos</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/meus-dados"><i class="fas fa-clipboard"></i> Meus dados</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/alterar-senha"> <i class="fas fa-lock-open"></i> Alterar senha</a></li>
            <li><a href="<?php echo Util::caminho_projeto() ?>/logoff"><i class="fas fa-reply"></i>  Sair</a></li>
            <?php endif ?>
        </ul>
    </div>

</amp-sidebar>
