
<?php
require_once("../../class/Include.class.php");

$obj_site = new Site();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if(!$obj_usuario->verifica_usuario_logado() ):
  $caminho = Util::caminho_projeto(). "/mobile/produtos";
  header("location: $caminho ");

endif;




// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input .btn_formulario .btn-lg .pull-right {
    display: none;
  }


  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>

</head>

<body class="bg-interna">
  <?php
  $voltar_para = 'locacao-de-equipamentos'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>




  <div class="row  bg_amarelo top15">
      <div class="col-2 text-center pt10">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/meus-dados">
              <i class="fas fa-user-edit fa-2x" style="color: #fff; opacity: .5"></i>
          </a>
      </div>
      <div class="col-3 linha_branca top25"></div>
      <div class="col-2 text-center pt10">
          <a disabled href="<?php echo Util::caminho_projeto() ?>/mobile/meus-pedidos">
              <i class="fas fa-clipboard-list fa-2x" style="color: #fff;opacity: .5; ;"></i>
          </a>
      </div>
      <div class="col-3 linha_branca top25"></div>
      <div class="col-2 text-center pt10">
          <a href="#">
              <i class="fas fa-lock-open fa-2x" style="color: #fff; "></i>
          </a>
      </div>
  </div>

  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row ">
      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 35) ?>

      <div class="col-12 empresa_geral top30">
          <h6><?php Util::imprime($row[legenda_1]); ?> <span><?php Util::imprime($row[legenda_2]); ?></span></h6>
      </div>

  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->






  <div class="row">



    <!--  ==============================================================  -->
    <!-- meus dados-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">

      <?php
      if(isset($_GET[btn_cadastrar])):

        $obj_usuario->atualiza_dados($_GET);

        echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Senha atualizado com sucesso.</div>";

      endif;
      ?>

      <form method="get" class="p2" action="#" target = "_top" >



        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha" data-minlength="6"    class="input-form input100 block border-none p0 m0" placeholder="SENHA NOVA" required>
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha2"    class="input-form input100 block border-none p0 m0" placeholder="CONFIRMAR SENHA NOVA" required>
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>



      <div class="col-12 text-right">
        <input type="submit"
        value="ENVIAR"
        name="btn_cadastrar"
        class="btn btn-block btn_topo">

      </div>

  </form>
</div>
<!--  ==============================================================  -->
<!--  meus dados-->
<!--  ==============================================================  -->

</div>



  <!--  ==============================================================  -->
  <!--   CONTATO E MAPA -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/contatos_e_mapa.php") ?>
  <!--  ==============================================================  -->
  <!--   CONTATO E MAPA -->
  <!--  ==============================================================  -->





  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>
