<?php
header('AMP-Access-Control-Allow-Source-Origin: http://localhost');

require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if (!$obj_usuario->verifica_usuario_logado()):
    $caminho = Util::caminho_projeto() . "/mobile/locacao-de-equipamentos";
    header("location: $caminho ");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if (!isset($_SESSION[produtos])):
    $caminho = Util::caminho_projeto() . "/mobile/locacao-de-equipamentos";
    header("location: $caminho ");
endif;


# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if (isset($_GET[btn_cadastrar])):
    $obj_carrinho->armazena_mensagem_endereco_entrega($_GET);

    $caminho = Util::caminho_projeto() . "/mobile/pagamento";
    header("location: $caminho ");

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html amp lang="pt-br">
<head>


    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body>
<?php
$voltar_para = 'carrinho'; // link de volta, exemplo produtos, dicas, servicos etc
$exibir_link = 'produtos';
require_once("../includes/topo.php");

?>


<div class="row  bg_amarelo top15">
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="fas fa-user fa-2x" style="color: #fff;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff;opacity: .5;"></i>
    </div>
</div>

<div class="row">
    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 32); ?>
    <div class="col-12  empresa_geral">
        <h6><?php Util::imprime($banner[legenda_1]); ?> <span><?php Util::imprime($banner[legenda_2]); ?></span></h6>
    </div>
</div>


<div class="row">


    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA -->
    <!--  ==============================================================  -->
    <div class="col-12 padding0">
        <form method="get" action="#" target="_top">


            <div class="ampstart-input inline-block  form_contatos m0 p0 mb3">

                <div class="col-12 top20">
                    <div class="relativo  input100">
                        <p>BAIRRO DA ENTREGA:
                            <b><?php Util::imprime(Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo")) ?></b>
                        </p>
                    </div>
                </div>


                <div class="clearfix"></div>


                <div class="col-12">
                    <div class="relativo  top20">
                        <input type="text" name="nome_contato" class="input-form input100 block border-none p0 m0"
                               placeholder="NOME DESTINATÁRIO" required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>
                </div>


                <div class="col-6 top10">
                    <div class="relativo ">
                        <input type="text" name="telefone_contato" class="input-form input100 block border-none p0 m0"
                               placeholder="TELEFONE">
                        <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>

                    </div>
                </div>


                <div class="col-6 top10">
                    <div class="relativo  ">
                        <input type="text" name="celular_contato" class="input-form input100 block border-none p0 m0"
                               placeholder="CELULAR " required>
                        <span class="fa fa-mobile form-control-feedback"></span>
                    </div>
                </div>


                <div class="col-8 top10">
                    <div class="relativo  ">
                        <input type="text" maxlength="8" name="cep_entrega"
                               class="input-form input100 block border-none p0 m0" placeholder="CEP" required>
                        <span class="fa fa-map-marker form-control-feedback"></span>
                    </div>
                </div>

                <div class="col-4 top10">
                    <div class="relativo  ">
                        <input type="text" name="numero_entrega" class="input-form input100 block border-none p0 m0"
                               placeholder="NR">
                        <span class="fa fa-calculator form-control-feedback"></span>

                    </div>
                </div>


                <div class="col-12 top10">
                    <div class="relativo  ">
                        <input type="text" name="endereco_entrega" class="input-form input100 block border-none p0 m0"
                               placeholder="ENDEREÇO" required>
                        <span class="fa fa-address-book form-control-feedback"></span>
                    </div>
                </div>



                <div class="col-12 top10">
                    <div class="relativo ">
                        <input type="text" name="complemento_entrega"
                               class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO">
                        <span class="fa fa-list form-control-feedback"></span>
                    </div>
                </div>

                <div class="col-12 top10">
                    <div class="relativo ">
                        <input type="text" name="cidade" disabled class="input-form input100 block border-none p0 m0"
                               placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo")); ?>">
                        <span class="far fa-building form-control-feedback"></span>
                    </div>
                </div>



                <div class="col-8 top10">
                    <div class="relativo ">
                        <input type="text" name="cidade" disabled class="input-form input100 block border-none p0 m0"
                               placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_cidade], "tb_cidades", "idcidade", "titulo")); ?>">
                        <span class="fas fa-globe form-control-feedback"></span>
                    </div>
                </div>


                <div class="col-4 top10">
                    <div class="relativo ">
                        <input type="text" name="uf" disabled class="input-form input100 block border-none p0 m0"
                               placeholder="<?php echo Util::imprime(Util::troca_value_nome($_SESSION[id_cidade], "tb_cidades", "idcidade", "uf")); ?>">
                        <span class="fa fa-list form-control-feedback"></span>
                    </div>
                </div>


                <div class="clearfix"></div>


                <div class="col-12 top10">
                    <div class="relativo ">
                        <input type="text" name="ponto_referencia" class="input-form input100 block border-none p0 m0"
                               placeholder="PONTO DE REFERÊNCIA">
                        <span class="fa fa-map-marker form-control-feedback"></span>

                    </div>
                </div>


                <div class="clearfix"></div>


            </div>

            <div class="col-12 top10">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/carrinho"
                   class="btn btn_orcamento btn_verde btn-block" title="Alterar bairro">ALTERAR ENDEREÇO DE ENTREGA</a>
            </div>


            <div class="col-12 top5 text-right ">
                <input type="submit" name="btn_cadastrar" id="btn_enviar" class="btn btn-lg btn-block btn-outline-secondary btn_detalhe_produtos btn_login" value="CADASTRAR"/>


            </div>

        </form>

        <!--  ==============================================================  -->
        <!-- LOCAL DA ENTREGA-->
        <!--  ==============================================================  -->


    </div>
</div>


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
