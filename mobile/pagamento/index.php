<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
$obj_usuario = new Usuario();
$obj_carrinho = new Carrinho();


# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if (!$obj_usuario->verifica_usuario_logado()):
    $caminho = Util::caminho_projeto() . "/mobile/locacao-de-equipamentos";
    header("location: $caminho");

endif;


# ==============================================================  #
# VERIFICO SE O CARRINHO ESTA COM INICIADO
# ==============================================================  #
if (!isset($_SESSION[produtos])):
    $caminho = Util::caminho_projeto() . "/mobile/locacao-de-equipamentos";
    header("location: $caminho");
endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>

    <?php require_once("../includes/head.php"); ?>


    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


    </style>

    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body>
<?php
$voltar_para = 'endereco-entrega'; // link de volta, exemplo produtos, dicas, servicos etc
$exibir_link = 'produtos';
require_once("../includes/topo.php");

?>


<div class="row  bg_amarelo top15">
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff;opacity: .5;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
    </div>
    <div class="col-3 linha_branca top25"></div>
    <div class="col-2 text-center pt10">
        <i class="far fa-credit-card fa-2x" style="color: #fff;"></i>
    </div>
</div>


<div class="row top25">
    <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 30); ?>
    <div class="col-12  empresa_geral">
        <h6><?php Util::imprime($banner[legenda_1]); ?> <span><?php Util::imprime($banner[legenda_2]); ?></span></h6>
    </div>
</div>

<div class="row">

    <div class="col-12 produtos_destaques top30">
        <blockquote>
            <h5><b>LOCAL DA ENTREGA
                    : <?php Util::imprime(Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo")) ?></b>
            </h5>
        </blockquote>
    </div>

    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->
    <div class="col-12  ">

        <h6 class="top10">Selecione o Método de Pagamento:</h6>

        <a href="<?php echo Util::caminho_projeto() ?>/pagamento/envia.php?tipo_pagamento=pagseguro"
           class="btn btn-lg btn-block btn-outline-secondary btn_detalhe_produtos btn_login">PAGSEGURO</a>
        <a href="<?php echo Util::caminho_projeto() ?>/pagamento/envia.php?tipo_pagamento=paypal" class="btn btn-lg btn-block btn-outline-secondary btn_detalhe_produtos btn_login top10">PAYPAL</a>


    </div>
    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->


</div>


<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->
<?php require_once("../includes/contatos_e_mapa.php") ?>
<!--  ==============================================================  -->
<!--   CONTATO E MAPA -->
<!--  ==============================================================  -->


<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
