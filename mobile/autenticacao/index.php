<?php

require_once("../../class/Include.class.php");
$obj_site = new Site();

$obj_usuario = new Usuario();


//session_destroy();

# ==============================================================  #
# VERIFICA SE O USUARIO JA ESTA LOGADO
# ==============================================================  #
if( $obj_usuario->verifica_usuario_logado() ):

  Util::script_location( Util::caminho_projeto() . "/endereco-entrega" );

endif;


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




?>
<!doctype html>
<html amp lang="pt-br">
<head>


  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

    </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>

</head>

<body>
  <?php
  $voltar_para = 'carinho'; // link de volta, exemplo produtos, dicas, servicos etc
  $exibir_link = 'produtos';
  require_once("../includes/topo.php");

  ?>



  <div class="row  bg_amarelo top15">
      <div class="col-2 text-center pt10">
          <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
      </div>
      <div class="col-3 linha_branca top25"></div>
      <div class="col-2 text-center pt10">
          <i class="fas  fa-user fa-2x" style="color: #fff; "></i>
      </div>
      <div class="col-3 linha_branca top25"></div>
      <div class="col-2 text-center pt10">
          <i class="fas fa-check-circle fa-2x" style="color: #fff; opacity: .5;"></i>
      </div>
  </div>


  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row ">
      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 28) ?>

      <div class="col-12 empresa_geral top30">
          <h6><?php Util::imprime($row[legenda_1]); ?> <span><?php Util::imprime($row[legenda_2]); ?></span></h6>
      </div>

  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->







  <div class="row">


    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12 padding0 alt_formulario">



      <?php
      # ==============================================================  #
      # VERIFICO SE E PARA EFETUAR O LOGIN
      # ==============================================================  #
      if(isset($_GET[btn_login])):

        if($obj_usuario->verifica_usuario($_GET[email], $_GET[senha])):

          if (isset($_SESSION[produtos])) {
            Util::script_location(Util::caminho_projeto()."/mobile/endereco-entrega");
          }else{
            Util::script_location(Util::caminho_projeto()."/mobile/locacao-de-equipamentos");
          }

        else:
          echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Usuário ou senha inválido.</div>";
        endif;

      endif;
      ?>


      <form method="get" class="p2" action="#" target = "_top" >


        <div class="col-12 ampstart-input inline-block  top30 form_contatos m0 p0 mb3">

          <div class="relativo">
            <input type="email" name="email" class="input-form input100 block border-none p0 m0"  placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>


          <div class="relativo">
            <input type="password" name="senha" class="input-form input100 block border-none p0 m0" placeholder="SENHA" required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>



          <div class="col-12 top10">
            <a class="btn1" href="<?php echo Util::caminho_projeto() ?>/mobile/recuperar-senha" title="Esqueci minha senha">
              <h6><b>Esqueci minha senha</b></h6>
            </a>
          </div>

          <div class="col-12">
            <input type="submit"
            value="ENVIAR"
            name="btn_login"
            class="btn btn-lg top5 btn-block btn_topo btn_login">
          </div>

        </div>


      </form>
    </div>

    <!--  ==============================================================  -->
    <!-- SOU CADASTRADO-->
    <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!-- NAO SOU CADASTRADO-->
    <!--  ==============================================================  -->
    <div class="col-12  padding0 fundo-formulario">
      <div class="col-12 empresa_geral top50">
        <h6>CRIAR NOVA <span>CONTA</span></h6>
      </div>

      <?php
      # ==============================================================  #
      # VERIFICO SE E PARA CADASTRAR O USUARIO
      # ==============================================================  #
      if(isset($_GET[btn_cadastrar])):

        if($obj_usuario->cadastra_usuario($_GET) != false)
        {
          Util::script_location(Util::caminho_projeto() . "/mobile/autenticacao");
        }
        else
        {
          echo "<div class='col-12 text-center top15 bottom15 aviso-erro'>Esse email já está cadastrado.</div>";
        }

      endif;
      ?>
      <form method="get" class="p2" action="#" target = "_top" >

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="nome" class="input-form input100 block border-none p0 m0" placeholder="NOME">
            <span class="fa fa-user form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="email" class="input-form input100 block border-none p0 m0" placeholder="E-MAIL">
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
        </div>



        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_residencial" class="input-form input100 block border-none p0 m0" placeholder="TELEFONE">
            <span class="fa fa-phone form-control-feedback" data-fa-transform="rotate-90"></span>
          </div>
        </div>

        <div class="col-6 top10">
          <div class="relativo">
            <input type="text" name="tel_celular" class="input-form input100 block border-none p0 m0" placeholder="CELULAR">
            <span class="fa fa-mobile form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="endereco" class="input-form input100 block border-none p0 m0" placeholder="ENDEREÇO">
            <span class="fa fa-address-book form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="numero" class="input-form input100 block border-none p0 m0" placeholder="NÚMERO">
            <span class="fa fa-calculator form-control-feedback"></span>
          </div>
        </div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="complemento" class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO">
            <span class="fa fa-list form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="text" name="bairro" class="input-form input100 block border-none p0 m0" placeholder="BAIRRO">
            <span class="fa fa-home form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-7 top10">
          <div class="relativo">
            <input type="text" name="cidade" class="input-form input100 block border-none p0 m0" placeholder="CIDADE">
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="col-5 top10">
          <div class="relativo">
            <input type="text" name="uf" max="2" class="input-form input100 block border-none p0 m0" placeholder="UF">
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha" class="input-form input100 block border-none p0 m0" placeholder="SENHA">
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 top10">
          <div class="relativo">
            <input type="password" name="senha2" class="input-form input100 block border-none p0 m0" placeholder="CONFIRMAR SENHA">
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
        </div>

        <div class="col-12 bottom50 text-right">
          <div class="top15 bottom25">
            <button type="submit" class="btn btn-lg top5 btn-block btn_topo btn_login" name="btn_cadastrar">
              CADASTRAR
            </button>
          </div>
        </div>

      </form>
    </div>
    <!--  ==============================================================  -->
    <!--  NAO SOU CADASTRADO-->
    <!--  ==============================================================  -->

  </div>





  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>
