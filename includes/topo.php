<?php
$obj_carrinho = new Carrinho();
$obj_usuario = new Usuario();


# ==============================================================  #
# VERIFICO SE O USUARIO ESTA LOGADO
# ==============================================================  #
if (isset($_POST[btn_enviar])):

    //  ARAMAZENO O LOCAL DE ENTREGA
    $_SESSION[id_bairro_entrega] = $_POST[bairro];
    $_SESSION[id_cidade] = $_POST[cidade];


    if (!isset($_SESSION[usuario])):
        Util::script_location(Util::caminho_projeto() . "/autenticacao");
    else:
        Util::script_location(Util::caminho_projeto() . "/endereco-entrega");
    endif;


endif;


# ==============================================================  #
# VERIFICO SE E PARA FINALIZAR A COMPRA
# ==============================================================  #
if (isset($_POST[btn_atualizar]) or isset($_POST[bairro])):

    $obj_carrinho->atualiza_itens($_POST['qtd']);
    $_SESSION[id_cidade] = $_POST[cidade];

endif;


# ==============================================================  #
# VERIFICO A ACAO DESEJADA GET
# ==============================================================  #
if (isset($_GET[action])):

    $action = base64_decode($_GET[action]);
    $id = base64_decode($_GET[id]);

    //  ESCOLHO A OPCAO
    switch ($action):

        case 'del':
            $obj_carrinho->del_item($id);
            break;

    endswitch;

endif;


# ==============================================================  #
# VERIFICO SE E PARA ADICIONAR UM ITEM
# ==============================================================  #
if (isset($_GET[action]) and $_GET[action] = 'add'):

    $obj_carrinho->add_item($_GET['idproduto']);

endif;


?>


<div class="container-fluid  bg_topo">
    <div class="row">


        <div class="container ">
            <div class="row">
                <?php
                if (empty($voltar_para)) {
                    $link_topo = Util::caminho_projeto() . "/";
                } else {
                    $link_topo = Util::caminho_projeto() . "/" . $voltar_para;
                }
                ?>

                <div class="col-2 top10">
                    <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $config[logomarca_desktop] ?>"
                             alt="início" class="">
                    </a>
                </div>


                <div class="col-10">
                    <div class="row top5">


                        <!-- ======================================================================= -->
                        <!-- telefones  -->
                        <!-- ======================================================================= -->
                        <div class="col-2 p-0">
                            <div class="btn btn-block btn_atendimento">ATENDIMENTO</div>
                        </div>
                        <div class="col-3 telefone_topo text-center top5">
                            <i class="fas fa-phone fa-fw mr-3" data-fa-transform="rotate-90"></i>
                            <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                        </div>

                        <?php if (!empty($config[telefone2])): ?>
                            <div class="col-3 text-center telefone_topo top5">
                                <i class="fab fa-whatsapp mr-3"></i>
                                <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                            </div>
                        <?php endif;

                        ?>



                        <?php /*
              <?php if (!empty($config[telefone3])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
              </div>
              <?php endif; ?>

              <?php if (!empty($config[telefone4])): ?>
              <div class="col-3 text-center telefone_topo">
              <i class="fab fa-whatsapp right10"></i>
              <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
              </div>
              <?php endif; ?>
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              */ ?>


                        <!--  ==============================================================  -->
                        <!--LOGIN -->
                        <!--  ==============================================================  -->
                        <div class="col-2 menu-topo-usuario">
                            <div class="dropdown">
                                <?php if (!isset($_SESSION[usuario])): ?>
                                    <button class="btn btn-secondary btn-block btn_login" type="button"
                                            id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        CONTA <i class="far fa-user ml-2"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"
                                           href="<?php echo Util::caminho_projeto() ?>/autenticacao">
                                            <i class="fas fa-sign-in-alt"></i> Entrar
                                        </a>
                                    </div>


                                <?php else: ?>
                                    <button class="btn btn-secondary btn_login btn-block" type="button"
                                            id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php Util::imprime($_SESSION[usuario][nome], 12) ?>
                                        <i class="far fa-user"></i>
                                    </button>


                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"
                                           href="<?php echo Util::caminho_projeto() ?>/meus-pedidos">
                                            <i class="fas fa-edit"></i> Meus pedidos
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo Util::caminho_projeto() ?>/meus-dados">
                                            <i class="fas fa-clipboard"></i> Meus dados
                                        </a>


                                        <a class="dropdown-item"
                                           href="<?php echo Util::caminho_projeto() ?>/alterar-senha">
                                            <i class="fas fa-lock-open"></i> Alterar senha
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo Util::caminho_projeto() ?>/logoff">
                                            <i class="fas fa-reply"></i> Sair
                                        </a>

                                    </div>
                                <?php endif ?>
                            </div>


                        </div>
                        <!--  ==============================================================  -->
                        <!--LOGIN -->
                        <!--  ==============================================================  -->


                        <!--  ==============================================================  -->
                        <!--CARRINHO-->
                        <!--  ==============================================================  -->
                        <div class=" col-2 dropdown text-center">
                            <a class="btn btn_atendimento nav-link1" href="#"
                               id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                CARRINHO <i class="fas fa-shopping-cart"></i>
                            </a>
                            <div class="dropdown-menu tabela_carrinho topo-meu-orcamento"
                                 aria-labelledby="navbarDropdownMenuLink">


                                <?php if (count($_SESSION[produtos]) == 0): ?>

                                   <div class="row">

                                       <div class="col-8">
                                           <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos"
                                              class="btn btn btn-outline-secondary rounded-0 btn_detalhe_produtos">
                                               <i class="fa fa-shopping-cart right10" aria-hidden="true"></i> CONTINUAR
                                               COMPRANDO <i class="fa fa-angle-right left10" aria-hidden="true"></i>
                                           </a>
                                       </div>
                                       <div class="col-4">
                                           <a href="<?php echo Util::caminho_projeto() ?>/contato"
                                              class="btn btn btn-outline-secondary rounded-0 btn_detalhe_produtos">
                                               CONTATO
                                           </a>
                                       </div>
                                   </div>

                                <?php else: ?>


                                    <table class="col-12 table   table-condensed">


                                        <form action="<?php echo Util::caminho_projeto(); ?>/carrinho/" method="post"
                                              name="form_produto_final" id="form_produto_final">

                                            <tbody>

                                            <?php foreach ($_SESSION[produtos] as $key => $dado_top): ?>
                                                <tr>
                                                    <td>
                                                        <?php $obj_site->redimensiona_imagem("../uploads/$dado_top[imagem]", 50, 50, array('alt' => $dado_top[titulo])); ?>
                                                    </td>
                                                    <td align="left pt-2" class="col-9">
                                                        <?php Util::imprime($dado_top[titulo]) ?>
                                                    </td>


                                                    <td class="col-3 text-center">
                                                        <h6>R$ <?php echo Util::formata_moeda($dado_top[preco]) ?></h6>
                                                    </td>

                                                    <td class="text-center">
                                                        <a class="btn btn-vermelho"
                                                           href="?id=<?php echo base64_encode($key) ?>&action=<?php echo base64_encode("del") ?>"
                                                           data-toggle="tooltip" data-placement="top" title="Excluir">
                                                            <i class="fas fa-times mr-1" style="color:  darkred;"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                                <?php $total_top += $dado_top[preco] * $dado_top[qtd]; ?>
                                            <?php endforeach; ?>

                                            </tbody>
                                    </table>

                                    <div class="col-12 text-right relativo">
                                        <input type="submit" name="btn_enviar" id="btn_enviar"
                                               class="btn btn-outline-secondary rounded-0 btn_detalhe_produtos"
                                               value="FINALIZAR PEDIDO"/>
                                    </div>

                                    </form>


                                <?php endif; ?>


                            </div>
                        </div>
                        <!--  ==============================================================  -->
                        <!--CARRINHO-->
                        <!--  ==============================================================  -->


                    </div>


                    <div class="col-12 menu_topo p-0 text-center">

                        <!--  ==============================================================  -->
                        <!-- MENU-->
                        <!--  ==============================================================  -->
                        <nav class="navbar  navbar-expand  navbar-light p-0">

                            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                <ul class="navbar-nav col justify-content-end">

                                    <li class="nav-item <?php if (Url::getURL(0) == "") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/">
                                            <h6>HOME</h6>
                                            <span class="sr-only">(current)</span>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "empresa") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/empresa">
                                            <h6>A EMPRESA </h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "locacao-de-equipamentos" or Url::getURL(0) == "locacao-de-equipamento") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos">
                                            <h6>LOCAÇÃO DE EQUIPAMENTOS</h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "assistencias-tecnicas" or Url::getURL(0) == "assistencia-tecnica") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/assistencias-tecnicas">
                                            <h6>ASSISTÊNCIA TÉCNICA</h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "dicas" or Url::getURL(0) == "dica") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/dicas">
                                            <h6>DICAS</h6>
                                        </a>
                                    </li>

                                    <li class="nav-item <?php if (Url::getURL(0) == "contato") {
                                        echo "active";
                                    } ?>">
                                        <a class="nav-link"
                                           href="<?php echo Util::caminho_projeto() ?>/contato">
                                            <h6>CONTATO</h6>
                                        </a>
                                    </li>


                            </div>


                    </div>


                </div>


            </div>

        </div>


    </div>

</div>


