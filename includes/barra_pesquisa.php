
<!-- ======================================================================= -->
<!-- BARRA PESQUISA -->
<!-- ======================================================================= -->
<div class="col-4">
    <form action="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos/" method="post">

        <div class="input-group mb-3">
            <input type="text" name="busca_produtos"
                   class="form-control btn_buscar border-right-0 rounded-0"
                   placeholder="BUSCAR PRODUTOS" aria-label="BUSCAR PRODUTOS"
                   aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary btn_buscar border-left-0 rounded-0"
                        type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>
</div>

<div class="col-2">

    <a href="<?php echo Util::caminho_projeto() ?>/contato" class="btn btn-block btn-outline-secondary rounded-0 btn_detalhe_produtos  btn_ligue">
        LIGUE AGORA
    </a>


</div>

<!-- ======================================================================= -->
<!-- BARRA PESQUISA    -->
<!-- ======================================================================= -->
