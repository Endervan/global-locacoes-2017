
<?php if (Url::getURL(1) == 'contato'): ?>
<button class="btn btn-block btn-outline-secondary rounded-0 btn_atendimento btn_ligue"
        type="button"
        data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
        aria-controls="collapseExample">
    TELEVENDAS
</button>
<?php endif; ?>

<div class="collapse disabled show" id="collapseExample">

    <div class="row border_pagina descricao_interna top20">

        <div class="col-12">
            <h6 class="">COMPRE PELO ATENDIMENTO</h6>
            <h6><span>ATENDIMENTO</span></h6>
            <h2><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h2>
        </div>


        <?php if (!empty($config[telefone2])): ?>
            <div class="col-12 top20 ">
                <h6><span>WHATTSAPP</span> <i class="fab fa-whatsapp right10 fa-2x"></i></h6>
                <h2> <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h2>
            </div>
        <?php endif; ?>

        <?php if (!empty($config[telefone3])): ?>
            <div class="col-12">
                <h2> <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?></h2>
            </div>
        <?php endif; ?>


        <?php if (!empty($config[telefone4])): ?>
            <div class="col-12">
                <h2> <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?></h2>
            </div>
        <?php endif; ?>

    </div>


</div>