<div class="container-fluid form_pag">
    <div class="row"></div>
</div>


<div class="container-fluid rodape">
    <div class="row pb20">
        <div class="container">
            <div class="row">
                <div class="col-10">

                    <!-- MENU   ================================================================ -->
                    <!-- ======================================================================= -->
                    <nav class="navbar  navbar-expand  navbar-light">

                        <div class=" navbar-collapse collapse " id="navbarNavDropdown">
                            <ul class="navbar-nav">

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "") {
                                        echo "active";
                                    } ?> "
                                       href="<?php echo Util::caminho_projeto() ?>/">HOME
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "empresa") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "locacao-de-equipamentos" OR Url::getURL(0) == "locacao-de-equipamento") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamentos">LOCAÇÃO DE EQUIPAMENTOS
                                    </a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "assistencias-tecnicas" or Url::getURL(0) == "assistencia-tecnica") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/assistencias-tecnicas">ASSISTÊNCIA TÉCNICA
                                    </a>
                                </li>

                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "dicas" or Url::getURL(0) == "dica") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
                                    </a>
                                </li>


                                <li class="nav-item ">
                                    <a class="nav-link <?php if (Url::getURL(0) == "contato") {
                                        echo "active";
                                    } ?>"
                                       href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO
                                    </a>
                                </li>




                            </ul>
                        </div>
                    </nav>
                    <!-- ======================================================================= -->
                    <!-- MENU    -->
                    <!-- ======================================================================= -->


                </div>



                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->
                <div class="col-2 dropdown dropup text-center">
                    <a class="btn btn_atendimento btn-block nav-link1  "
                       href="<?php echo Util::caminho_projeto() ?>/carrinho">
                        CARRINHO <i class="fas fa-shopping-cart"></i>
                    </a>

                </div>
                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->

                <!-- ======================================================================= -->
                <!-- LOGO    -->
                <!-- ======================================================================= -->
                <div class="col-2 text-center top30">
                    <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início"/>
                    </a>
                </div>
                <!-- ======================================================================= -->
                <!-- LOGO    -->
                <!-- ======================================================================= -->


                <div class="col-8 p-0 top30">


                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->
                    <div class="col-10">
                        <div class="row borde_rodape telefone_topo">

                            <div class="col-3 p-0">
                                <div class="btn btn_atendimento btn-lg mr-4">ATENDIMENTO</div>
                            </div>
                            <div class="col-4  text-center">
                                <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
                                <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                                <span class="atentimento">ATENDIMENTO</span>
                            </div>

                            <?php if (!empty($config[telefone2])): ?>
                                <div class="col-4 text-left">
                                    <i class="fab fa-whatsapp right10"></i>
                                    <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                                    <span class="atentimento">WHATSAPP</span>
                                </div>
                            <?php endif;

                            ?>
                        </div>


                    </div>

                    <!-- ======================================================================= -->
                    <!-- telefones  -->
                    <!-- ======================================================================= -->

                    <?php if (!empty($config[endereco])): ?>
                        <div class="telefones top20">
                            <i class="fas fa-map-marker-alt mr-3"></i>
                            <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[endereco]); ?>
                        </div>
                    <?php endif; ?>


                </div>


                <!-- ======================================================================= -->
                <!-- redes sociais    -->
                <!-- ======================================================================= -->
                <div class="col-2 top60 p-0 redes">
                        <?php if ($config[facebook] != "") { ?>
                            <a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank">
                                <i class="fab fa-facebook-square fa-2x "></i>
                            </a>
                        <?php } ?>

                        <?php if ($config[instagram] != "") { ?>
                            <a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank">
                                <i class="fab fa-instagram fa-2x "></i>
                            </a>
                        <?php } ?>

                        <?php if ($config[google_plus] != "") { ?>
                            <a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus"
                               target="_blank">
                                <i class="fab fa-google-plus-g fa-2x "></i>
                            </a>
                        <?php } ?>

                        <a href="http://www.homewebbrasil.com.br" target="_blank">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
                        </a>

                </div>
                <!-- ======================================================================= -->
                <!-- redes sociais    -->
                <!-- ======================================================================= -->


            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row rodape_baixo">
        <div class="container">
            <div class="row ">
                <div class="col-12 text-center top10">
                    <h5> © Copyright GLOBAL LOCAÇÕES</h5>
                </div>
            </div>
        </div>
    </div>
</div>
