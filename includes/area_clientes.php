<div class="col-3  lista-passos-clientes">

    <div class="list-group text-center">
        <a href="<?php echo Util::caminho_projeto() ?>/meus-pedidos" class="list-group-item btn-lg
        <?php if (Url::getURL(0) == "meus-pedidos") {
            echo "active";
        } ?>">

            MEUS PEDIDOS  <i class="fas fa-edit pull-right" ></i>
        </a>

        <a href="<?php echo Util::caminho_projeto() ?>/meus-dados" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "meus-dados") {
            echo "active";
        } ?>">

             MEUS DADOS <i class="fas fa-clipboard pull-right"></i>
        </a>

        <a href="<?php echo Util::caminho_projeto() ?>/alterar-senha" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "alterar-senha") {
            echo "active";
        } ?>">

           ALTERAR SENHA  <i class="fas fa-lock-open pull-right"></i>
        </a>


    </div>

    <?php require_once ('./includes/televendas.php');?>
</div>
