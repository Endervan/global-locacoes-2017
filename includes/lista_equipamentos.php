<?php
if (mysql_num_rows($result1) > 0) {
    while ($row = mysql_fetch_array($result1)) {
        ?>
        <div class="col-3 produtos">
            <div class="card top45">
                <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamento/<?php Util::imprime($row[url_amigavel]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 268, 200, array("class" => "w-100", "alt" => "$row[titulo]")) ?>
                </a>
                <div class="card-body">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
                    <div class="desc_prod_geral"><h6 class="card-title"><?php Util::imprime($row[titulo]); ?></h6></div>
                    </a>
                        <h6 class="text-right">R$ <?php echo Util::formata_moeda($row[preco]); ?></h6>

                </div>


            </div>

            <div class="row">
                <div class="col-5 pr-0">
                    <a href="<?php echo Util::caminho_projeto() ?>/locacao-de-equipamento/<?php Util::imprime($row[url_amigavel]); ?>"
                       class="btn btn-block btn-outline-secondary btn_detalhe_produtos" role="button" aria-pressed="true">
                        <i class="fas fa-times mr-1" data-fa-transform="rotate-45"></i> MAIS
                    </a>
                </div>

                <div class="col-7 pl-0">
                    <a href="<?php echo Util::caminho_projeto() ?>/carrinho/?idproduto=<?php echo Util::imprime($row[idproduto]) ?>&action=add"
                       class="btn btn-block btn-outline-secondary btn_detalhe_produtos btn_compra" role="button" aria-pressed="true">
                        COMPRAR AQUI
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

