<?php
if (mysql_num_rows($result) > 0) {
    $i = 0;
    while ($row = mysql_fetch_array($result)) {

        ?>

    <div class="col-3 p-0 top5 relativo">
        <div class="lista-produto portfolio">
            <div class="grid">
                <figure class="effect-julia">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 204, 138,
                        array("class" => "w-100", "alt" => "$row[titulo]")) ?>
                    <figcaption>
                        <a class="btn btn-block"
                           href="<?php echo Util::caminho_projeto() ?>/assistencia-tecnica/<?php echo Util::imprime($row[url_amigavel]) ?>"
                           title="SAIBA MAIS">
                            <?php /*
 <i class="fas fa-plus-circle fa-3x hover_icon"></i>*/ ?>
                        </a>
                    </figcaption>
                </figure>

            </div>
            <div class="col-12  servicos_body text-center pb-4">
                <div class="desc_servicos_geral">
                    <h6 class="text-left"><?php echo Util::imprime($row[titulo]) ?></h6>

                </div>

                <a href="<?php echo Util::caminho_projeto() ?>/assistencia-tecnica/<?php Util::imprime($row[url_amigavel]); ?>"
                   class="btn btn-outline-warning btn_detalhe_servicos top20" role="button" aria-pressed="true">
                    MAIS DETALHES
                </a>


            </div>


        </div>


    </div>


        <?php

        if ($i == 3) {
            echo '<div class="col-12 top150"></div>';
            $i = 0;
        } else {
            $i++;
        }

    }
}
?>

         