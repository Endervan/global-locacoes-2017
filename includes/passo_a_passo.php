<div class="col-3  lista-passos-carrinho">
    <div class="list-group text-center">
        <a href="javacript:void(0);" class="list-group-item btn-lg
        <?php if (Url::getURL(0) == "carrinho") {
            echo "active";
        } ?>">

            MEU CARRINHO <i class="fas fa-shopping-cart ml-auto" style="float: right"></i>
        </a>

        <a href="javacript:void(0);" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "autenticacao") {
            echo "active";
        } ?>">

            CADASTRO <i class="fas fa-user" style="float: right"></i>
        </a>

        <a href="javacript:void(0);" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "endereco-entrega") {
            echo "active";
        } ?>">

            ENDEREÇO <i class="fas fa-globe" style="float: right"></i>
        </a>

        <a href="javacript:void(0);" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "pagamento") {
            echo "active";
        } ?>">

            PAGAMENTO <i class="far fa-credit-card" style="float: right"></i>
        </a>

        <a href="javacript:void(0);" class="list-group-item btn-lg
         <?php if (Url::getURL(0) == "finalizado") {
            echo "active";
        } ?>">

            OBRIGADO <i class="fas fa-check" style="float: right"></i>
        </a>
    </div>

    <?php require_once ('./includes/televendas.php');?>
</div>
