<?php
ob_start();
session_start();


class Informacao_Model extends Dao
{

    private $nome_tabela = "tb_configuracoes";
    private $chave_tabela = "idconfiguracao";
    public $obj_imagem;


    /*	==================================================================================================================	*/
    #	CONSTRUTOR DA CLASSE
    /*	==================================================================================================================	*/
    public function __construct()
    {
        $this->obj_imagem = new Imagem();
        parent::__construct();
    }



    /*	==================================================================================================================	*/
    /*	FORMULARIO	*/
    /*	==================================================================================================================	*/
    public function formulario($dados)
    {
        ?>

        <input style="display: none;" type="text" name="endereco" value="<?php Util::imprime($dados[endereco]) ?>"
               class="form-control fundo-form1 input100">


        <div class="col-xs-12">
            <div class="alert alert-info">
                ZOPIM
            </div>
        </div>

        <div class="col-xs-12 form-group ">
            <label>SRC ZOPIM <span></span></label>
            <input type="text" name="src_zopim" value="<?php Util::imprime($dados[src_zopim]) ?>"
                   class="form-control fundo-form1 input100">
        </div>


        <div class="col-xs-12">
            <div class="alert alert-info">
                IMAGENS DESKTOP
            </div>
        </div>

        <div class="col-xs-4 form-group ">
            <label>Logomarca Desktop <span>250x45px</span></label>
            <?php
            if (!empty($dados[logomarca_desktop])) {
                ?>
                <input type="file" name="logomarca_desktop" id="logomarca_desktop"
                       value="<?php Util::imprime($dados[logomarca_desktop]) ?>"
                       class="form-control fundo-form1 input100"/>
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-3"
                     src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[logomarca_desktop]) ?>"/>
            <?php } else { ?>
                <input type="file" name="logomarca_desktop" id="logomarca_desktop"
                       value="<?php Util::imprime($dados[logomarca_desktop]) ?>"
                       class="form-control fundo-form1 input100"/>
            <?php } ?>
        </div>



        <div class="col-xs-12">
            <div class="alert alert-info">
                IMAGENS MOBILE
            </div>
        </div>


        <div class="col-xs-4 form-group ">
            <label>Logomarca Mobile <span>160x50px</span></label>
            <?php
            if (!empty($dados[bg_mobile_paginas])) {
                ?>
                <input type="file" name="bg_mobile_paginas" id="bg_mobile_paginas"
                       value="<?php Util::imprime($dados[bg_mobile_paginas]) ?>"
                       class="form-control fundo-form1 input100"/>
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-6"
                     src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_mobile_paginas]) ?>"/>
            <?php } else { ?>
                <input type="file" name="bg_mobile_paginas" id="bg_mobile_paginas"
                       value="<?php Util::imprime($dados[bg_mobile_paginas]) ?>"
                       class="form-control fundo-form1 input100"/>
            <?php } ?>
        </div>

        <div class="col-xs-4 form-group ">
            <label>Imagem Mobile Index <span>360x550px</span></label>
            <?php
            if (!empty($dados[bg_mobile_index])) {
                ?>
                <input type="file" name="bg_mobile_index" id="bg_mobile_index"
                       value="<?php Util::imprime($dados[bg_mobile_index]) ?>"
                       class="form-control fundo-form1 input100"/>
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-2"
                     src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_mobile_index]) ?>"/>
            <?php } else { ?>
                <input type="file" name="bg_mobile_index" id="bg_mobile_index"
                       value="<?php Util::imprime($dados[bg_mobile_index]) ?>"
                       class="form-control fundo-form1 input100"/>
            <?php } ?>
        </div>





        <script>
            $(document).ready(function () {
                $('.FormPrincipal').bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-remove',
                        validating: 'fa fa-refresh'
                    },
                    fields: {

                        email: {
                            validators: {
                                notEmpty: {},
                                emailAddress: {
                                    message: 'Esse endereço de email não é válido'
                                }
                            }
                        },

                        <?php if($dados[logomarca_desktop] == ''){ ?>
                        logomarca_desktop: {
                            validators: {
                                notEmpty: {
                                    message: 'Por favor insira Uma Imagem'
                                },
                                file: {
                                    extension: 'png',
                                    maxSize: 1024 * 1024,   // 2 MB
                                    message: 'O arquivo selecionado não é valido, ele deve ser (PNG) e 2 MB no máximo.'
                                }
                            }
                        },
                        <?php } ?>


                        <?php if($dados[logomarca_mobile] == ''){ ?>
                        logomarca_mobile: {
                            validators: {
                                notEmpty: {
                                    message: 'Por favor insira Uma Imagem'
                                },
                                file: {
                                    extension: 'png',
                                    maxSize: 1024 * 1024,   // 2 MB
                                    message: 'O arquivo selecionado não é valido, ele deve ser (PNG) e 2 MB no máximo.'
                                }
                            }
                        },
                        <?php } ?>

                        mensagem: {
                            validators: {
                                notEmpty: {}
                            }
                        }
                    }
                });
            });
        </script>


        <?php
    }




    /*	==================================================================================================================	*/
    /*	FORMULARIO	*/
    /*	==================================================================================================================	*/
    public function formulario_email_smtp($dados)
    {
        ?>

        <div class="col-xs-12 bottom20">
            <h3>Configure o smtp para os email serem enviados pelo gmail.</h3>
        </div>


        <div class="col-xs-12 form-group ">
            <label>Email<span> Informe o email completo exemplo: email@gmail.com</span></label>
            <input type="text" name="email_gmail" id="email_gmail" value="<?php Util::imprime($dados[email_gmail]) ?>"
                   class="form-control fundo-form1 input100"/>
        </div>


        <div class="col-xs-12 form-group ">
            <label>Senha</label>
            <input type="text" name="senha_gmail" id="senha_gmail" value="<?php Util::imprime($dados[senha_gmail]) ?>"
                   class="form-control fundo-form1 input100"/>
        </div>


        <script>
            $(document).ready(function () {
                $('.FormPrincipal').bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-remove',
                        validating: 'fa fa-refresh'
                    },
                    fields: {

                        email_gmail: {
                            validators: {
                                notEmpty: {},
                                emailAddress: {
                                    message: 'Esse endereço de email não é válido'
                                }
                            }
                        },

                        senha_gmail: {
                            validators: {
                                notEmpty: {}
                            }
                        }
                    }
                });
            });
        </script>


        <?php
    }







    /*	==================================================================================================================	*/
    /*	EFETUA CROP DA IMAGEM	*/
    /*	==================================================================================================================	*/
    public function efetua_crop_imagem($id, $nome_arquivo, $nome_campo, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso, $tamanho_imagem = 593)
    {
        //	CRIO O CROP DA IMAGEM
        $nome_tabela = $this->nome_tabela;
        $idtabela = $this->chave_tabela;
        $this->obj_imagem->gera_imagem_crop($id, $nome_arquivo, $nome_tabela, $idtabela, $nome_campo, $tamanho_imagem, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso);
    }



    /*	==================================================================================================================	*/
    /*	EFETUA O CADASTRO	*/
    /*	==================================================================================================================	*/
    public function cadastra($dados)
    {
        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[logomarca_desktop][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            Util::upload_arquivo("../../uploads", $_FILES[logomarca_desktop], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[logomarca_mobile][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            Util::upload_arquivo("../../uploads", $_FILES[logomarca_mobile], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_paginas][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_paginas], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_index][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_index], "3145728");
        }


        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_empresa][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_mobile_empresa] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_empresa], "3145728");
        }

        //	CADASTRA O USUARIO
        $id = parent::insert($this->nome_tabela, $_POST);

        //	ARMAZENA O LOG
        parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


        Util::script_msg("Cadastro efetuado com sucesso.");
        Util::script_location(dirname($_SERVER['SCRIPT_NAME']) . "/cadastra.php");
    }



    /*	==================================================================================================================	*/
    /*	EFETUA A ALTERACAO	*/
    /*	==================================================================================================================	*/
    public function altera($id, $dados)
    {

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[logomarca_desktop][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[logomarca_desktop] = Util::upload_arquivo("../../uploads", $_FILES[logomarca_desktop], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[logomarca_mobile][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[logomarca_mobile] = Util::upload_arquivo("../../uploads", $_FILES[logomarca_mobile], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_paginas][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_mobile_paginas] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_paginas], "3145728");
        }


        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_empresa][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_empresa] = Util::upload_arquivo("../../uploads", $_FILES[bg_empresa], "3145728");
        }


        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_empresa_home][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_empresa_home] = Util::upload_arquivo("../../uploads", $_FILES[bg_empresa_home], "3145728");
        }

        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_index][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_mobile_index] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_index], "3145728");
        }


        //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
        if ($_FILES[bg_mobile_empresa][name] != "") {
            //	EFETUO O UPLOAD DA IMAGEM
            $dados[bg_mobile_empresa] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_empresa], "3145728");
        }

        parent::update($this->nome_tabela, $id, $dados);


        //	ARMAZENA O LOG
        parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


        Util::script_msg("Alterado com sucesso.");
        Util::script_location(dirname($_SERVER['SCRIPT_NAME']) . "/index.php");
    }



    /*	==================================================================================================================	*/
    /*	ATIVA OU DESATIVA	*/
    /*	==================================================================================================================	*/
    public function ativar_desativar($id, $ativo)
    {
        if ($ativo == "SIM") {
            $sql = "UPDATE " . $this->nome_tabela . " SET ativo = 'NAO' WHERE " . $this->chave_tabela . " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
        } else {
            $sql = "UPDATE " . $this->nome_tabela . " SET ativo = 'SIM' WHERE " . $this->chave_tabela . " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
        }

    }




    /*	==================================================================================================================	*/
    /*	EXCLUI	*/
    /*	==================================================================================================================	*/
    public function excluir($id)
    {
        //	BUSCA OS DADOS
        $row = $this->select($id);

        $sql = "DELETE FROM " . $this->nome_tabela . " WHERE " . $this->chave_tabela . " = '$id'";
        parent::executaSQL($sql);

        //	ARMAZENA O LOG
        parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
    }







    /*	==================================================================================================================	*/
    /*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO	*/
    /*	==================================================================================================================	*/
    public function verifica($email)
    {
        $sql = "SELECT * FROM " . $this->nome_tabela . " WHERE email = '$email'";
        return mysql_num_rows(parent::executaSQL($sql));
    }




    /*	==================================================================================================================	*/
    /*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO QUANDO ALTERAR	*/
    /*	==================================================================================================================	*/
    public function verifica_altera($email, $id)
    {
        $sql = "SELECT * FROM " . $this->nome_tabela . " WHERE email = '$email' AND " . $this->chave_tabela . " <> '$id'";
        return mysql_num_rows(parent::executaSQL($sql));
    }





    /*	==================================================================================================================	*/
    /*	BUSCA OS DADOS	*/
    /*	==================================================================================================================	*/
    public function select($id = "")
    {
        if ($id != "") {
            $sql = "
      SELECT
      *
      FROM
      " . $this->nome_tabela . "
      WHERE
      " . $this->chave_tabela . " = '$id'
      ";
            return mysql_fetch_array(parent::executaSQL($sql));
        } else {
            $sql = "
      SELECT
      *
      FROM
      " . $this->nome_tabela . "
      ORDER BY
      ordem desc
      ";
            return parent::executaSQL($sql);
        }

    }


}

?>
